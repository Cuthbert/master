package ma;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import ma.kernel.BasicKernel;

public class Options {
	public static final String SVM = "SVM";
//	public static final String SINGLE_SVM = "SINGLE_SVM";
	public static final String KNN = "KNN";
	
	public String inputFile;
	public String outputFile = "temp";
	public boolean refresh = false;
	public String databasePath;
	public int noOfTrainingInstances = 0;
	
	public String kernel = "0";
	public String[] kernelVariation = null;
	
	public BasicKernel kernelInstance = null;
	public boolean wait = false;
	public boolean normalize = false;
	public boolean[] normalizeVariation = {false,true};
	public String label;
	
	public double ratio = Double.NaN;
	public double[] ratioVariation = null;
	public int iterations = 0;
	
	public int expansionSize = 0;
	public int[] expansionSizeVariation = null;
	public double threshold = 0.0;
	public boolean logLines = false;
	public int smallerRatioLimit = -1;
	public double lambda=0.5;
	public double[] lambdaVaration = null;
	public boolean randomSelection = true;
	public int noOfTestinstances = 100;
	public int noOfValidationInstances = 100;
	public int noOfNewTrainingInstances = 1;
	public int[] noOfNewTrainingInstancesVariation = null;
	public boolean createAlternative = false;
	public int k = 15;
	
	public Properties setup = new Properties();
	public int outputInterval = 1000;
	public int commitInterval = 100;
	public int statsInterval = 10000;
	public String[] labelVariation = null;
	public double randomRatio = 0.0;
	public double[] randomRatioVariation = null;
	public int repeat = 1;
	
	public boolean persistantTestData = true;
	public double epsilon;
	public String classificator = Options.SVM;
	public String[] classificatorVariation = null;
	
	public int optionId = 0;
	public double kNNThreshold = 0.5;
	public boolean processLogging = false;
	
	public String setupName = "none";
	public int noOfPreviousTrainingInstances;
	public boolean isSimpleSVM = false;
	
	public int sentenceLimit = -1;
	
	public double svm_c = 1.0;
	public double[] SVMCVariation = null;
	
	public Options(){}
	
	/**
	 * Copy constructor. Avoids vary information
	 * @param options
	 */
	public Options(Options options) {
		inputFile = options.inputFile;
		outputFile = options.outputFile;
		refresh = options.refresh;
		databasePath = options.databasePath;
		noOfTrainingInstances = options.noOfTrainingInstances;
		kernel = options.kernel;
//		kernelVaration = options.kernelVaration;
		kernelInstance = options.kernelInstance;
		wait = options.wait;
		normalize = options.normalize;
		label = options.label;
		ratio = options.ratio;
//		ratioVaration = options.ratioVaration;
		iterations = options.iterations;
		expansionSize = options.expansionSize;
//		expansionSizeVaration = options.expansionSizeVaration;
		threshold = options.threshold;
		logLines = options.logLines;
		smallerRatioLimit = options.smallerRatioLimit;
		lambda = options.lambda;
//		lambdaVaration = options.lambdaVaration;
		randomSelection = options.randomSelection;
		noOfTestinstances = options.noOfTestinstances;
		noOfNewTrainingInstances = options.noOfNewTrainingInstances;
		noOfValidationInstances = options.noOfValidationInstances;
		setup = options.setup;
		randomRatio = options.randomRatio;
		repeat = options.repeat;
		epsilon = options.epsilon;
		classificator = options.classificator;
		k = options.k;
		kNNThreshold = options.kNNThreshold;
		
		outputInterval = options.outputInterval;
		commitInterval = options.commitInterval;
		statsInterval = options.statsInterval;
		
		persistantTestData = options.persistantTestData;
		processLogging = options.processLogging;
		isSimpleSVM = options.isSimpleSVM;
		
		sentenceLimit = options.sentenceLimit;
		svm_c = options.svm_c;
	}
	
	public String values() {
		String output = "";
		output+="Database path: "+databasePath+"\n";
		output+="Refresh flag (0|1): "+(refresh?"1":"0")+"\n";
		output+="Input file: "+inputFile+"\n";
//		output+="Output file / Prefix for output files: "+outputFile+"\n";
		output+="Limit # of entries: "+noOfTrainingInstances+"\n";
		output+="Kernel: "+kernel+" "+(kernelInstance!=null?kernelInstance.getClass().getName():"")+"\n";
		output+="label: "+label+"\n";
		output+="Ratio of positive classified examples: "+ratio+"\n";
		output+="# of maximum iterations: "+iterations+"\n";
		output+="# of entries to add per iteration: "+expansionSize+"\n";
		output+="Threshold: "+threshold+"\n";
		output+="Log classification details: "+(logLines?"1":"0")+"\n";
		output+="minimum # of consecutive smaller or equal ratios: "+smallerRatioLimit+"\n";
		output+="epsilon for accuracy and auc: "+epsilon+"\n";
		output+="lambda: "+lambda+"\n";
		output+="randomSelection: "+(randomSelection?"1":"0")+"\n";
		output+="# of test instances: "+noOfTestinstances+"\n";
		output+="# of new training instances: "+noOfNewTrainingInstances+"\n";
		output+="# of new validation instances: "+noOfValidationInstances+"\n";
		output+="Ratio of random new training instances: "+randomRatio+"\n";
		output+="Repeat the setup: "+repeat+"\n";
		output+="Classificator: "+classificator+"\n";
		output+="k: "+k+"\n";
		output+="kNNThreshold: "+kNNThreshold+"\n";
		output+="sentence limit per entry: "+sentenceLimit+"\n";
		output+="SMV C: "+svm_c+"\n";
		
		return output;
	}

	public void loadSetup(String fileName) {
		try {
			setup.load(new FileReader(fileName));
			setupName = (new File(fileName)).getName();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(setup.containsKey("setup.database")) databasePath = setup.getProperty("setup.database");
		if(setup.containsKey("setup.refresh")) refresh = setup.getProperty("setup.refresh").equals("1");
		if(setup.containsKey("setup.inputFile")) inputFile = setup.getProperty("setup.inputFile");
//		if(setup.containsKey("setup.outputFile")) outputFile = setup.getProperty("setup.outputFile");
		if(setup.containsKey("setup.readLimit")) noOfTrainingInstances = Integer.parseInt(setup.getProperty("setup.readLimit"));
		if(setup.containsKey("setup.kernelId")) kernel = setup.getProperty("setup.kernelId");
		if(setup.containsKey("setup.kernelId.vary")) kernelVariation = setup.getProperty("setup.kernelId.vary").split(",");
		if(setup.containsKey("setup.label")) label = setup.getProperty("setup.label");
		if(setup.containsKey("setup.label.vary")) labelVariation = setup.getProperty("setup.label.vary").split(",");
		if(setup.containsKey("setup.ratio")) ratio = Double.parseDouble(setup.getProperty("setup.ratio"));
		if(setup.containsKey("setup.ratio.vary")) {
			String[] tempRatioVaration = setup.getProperty("setup.ratio.vary").split(",");
			ratioVariation = new double[tempRatioVaration.length];
			for(int i=0; i<tempRatioVaration.length;i++) {
				ratioVariation[i] = Double.parseDouble(tempRatioVaration[i]);
			}
		}
		if(setup.containsKey("setup.iterations")) iterations = Integer.parseInt(setup.getProperty("setup.iterations"));
		if(setup.containsKey("setup.expansionSize")) expansionSize = Integer.parseInt(setup.getProperty("setup.expansionSize"));
		if(setup.containsKey("setup.expansionSize.vary")) {
			String[] tempExpansionVaration = setup.getProperty("setup.expansionSize.vary").split(",");
			expansionSizeVariation = new int[tempExpansionVaration.length];
			for(int i=0; i<tempExpansionVaration.length;i++) {
				expansionSizeVariation[i] = Integer.parseInt(tempExpansionVaration[i]);
			}
		}
		if(setup.containsKey("setup.threshold")) threshold = Double.parseDouble(setup.getProperty("setup.threshold"));
		if(setup.containsKey("setup.logLines")) logLines = setup.getProperty("setup.logLines").equals("1");
		if(setup.containsKey("setup.numberOfSmallerRatios")) smallerRatioLimit = Integer.parseInt(setup.getProperty("setup.numberOfSmallerRatios"));
		if(setup.containsKey("setup.epsilon")) epsilon = Double.parseDouble(setup.getProperty("setup.epsilon"));
		if(setup.containsKey("setup.lambda")) lambda = Double.parseDouble(setup.getProperty("setup.lambda"));
		if(setup.containsKey("setup.lambda.vary")) {
			String[] tempLambdaVaration = setup.getProperty("setup.lambda.vary").split(",");
			lambdaVaration = new double[tempLambdaVaration.length];
			for(int i=0; i<tempLambdaVaration.length;i++) {
				lambdaVaration[i] = Double.parseDouble(tempLambdaVaration[i]);
			}
		}
		if(setup.containsKey("setup.random")) randomSelection = setup.getProperty("setup.random").equals("1");
		if(setup.containsKey("setup.normalize")) normalize = setup.getProperty("setup.normalize").equals("1");
		if(setup.containsKey("setup.processLogging")) processLogging = setup.getProperty("setup.processLogging").equals("1");
		if(!setup.containsKey("setup.normalize.vary")) normalizeVariation = null;  
		if(setup.containsKey("setup.numberOfTestInstances")) noOfTestinstances = Integer.parseInt(setup.getProperty("setup.numberOfTestInstances"));
		if(setup.containsKey("setup.numberOfValidationInstances")) noOfValidationInstances = Integer.parseInt(setup.getProperty("setup.numberOfValidationInstances"));
		
		if(setup.containsKey("setup.numberOfNewTrainingInstances")) noOfNewTrainingInstances = Integer.parseInt(setup.getProperty("setup.numberOfNewTrainingInstances"));
		if(setup.containsKey("setup.numberOfNewTrainingInstances.vary")) {
			String[] tempTrainExpansionVaration = setup.getProperty("setup.numberOfNewTrainingInstances.vary").split(",");
			noOfNewTrainingInstancesVariation = new int[tempTrainExpansionVaration.length];
			for(int i=0; i<tempTrainExpansionVaration.length;i++) {
				noOfNewTrainingInstancesVariation[i] = Integer.parseInt(tempTrainExpansionVaration[i]);
			}
		}
		if(setup.containsKey("setup.ratioOfRandomNewTrainingInstances")) randomRatio = Double.parseDouble(setup.getProperty("setup.ratioOfRandomNewTrainingInstances"));
		if(setup.containsKey("setup.ratioOfRandomNewTrainingInstances.vary")) {
			String[] temprandomRatioVaration = setup.getProperty("setup.ratioOfRandomNewTrainingInstances.vary").split(",");
			randomRatioVariation = new double[temprandomRatioVaration.length];
			for(int i=0; i<temprandomRatioVaration.length;i++) {
				randomRatioVariation[i] = Double.parseDouble(temprandomRatioVaration[i]);
			}
		}
		if(setup.containsKey("setup.repeat")) repeat = Integer.parseInt(setup.getProperty("setup.repeat"));
		
		classificator = setup.getProperty("setup.classificator", Options.SVM);
		k = Integer.parseInt(setup.getProperty("setup.k", "9"));
		kNNThreshold = Double.parseDouble(setup.getProperty("setup.kNNThreshold", "0.5"));
		if(setup.containsKey("setup.classificator.vary")) classificatorVariation = setup.getProperty("setup.classificator.vary").split(",");
		
		sentenceLimit = Integer.parseInt(setup.getProperty("setup.sentenceLimit", "-1"));
		svm_c = Double.parseDouble(setup.getProperty("setup.svm_c", "1.0"));
		if(setup.containsKey("setup.svm_c.vary")) {
			String[] tempSVMCVaration = setup.getProperty("setup.svm_c.vary").split(",");
			SVMCVariation = new double[tempSVMCVaration.length];
			for(int i=0; i<tempSVMCVaration.length;i++) {
				SVMCVariation[i] = Double.parseDouble(tempSVMCVaration[i]);
			}
		}
	}
}
