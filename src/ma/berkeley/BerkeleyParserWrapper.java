package ma.berkeley;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import ma.annotations.Measure;
import ma.annotations.Trace;
import edu.berkeley.nlp.PCFGLA.BerkeleyParser.Options;
import edu.berkeley.nlp.PCFGLA.CoarseToFineMaxRuleParser;
import edu.berkeley.nlp.PCFGLA.CoarseToFineNBestParser;
import edu.berkeley.nlp.PCFGLA.Grammar;
import edu.berkeley.nlp.PCFGLA.Lexicon;
import edu.berkeley.nlp.PCFGLA.ParserData;
import edu.berkeley.nlp.syntax.Tree;
import edu.berkeley.nlp.util.Numberer;

public class BerkeleyParserWrapper {
	public static void main(String[] args) {
		BerkeleyParserWrapper wrapper = new BerkeleyParserWrapper();
		wrapper.init("data/parser/ger_sm5.gr");
		
		List<Tree<String>> result = wrapper.parse("Das ist ein komplizierter Testsatz mit einer kleinen Anmerkung.");
		PrintWriter outputData;
		try {
			outputData = new PrintWriter(new OutputStreamWriter(new FileOutputStream("data/output.log"), "UTF-8"),true);
			Iterator<Tree<String>> iterator = result.iterator();
			while(iterator.hasNext()) {
				outputData.write(iterator.next().toString()+"\n");
			}
//			outputData.flush();
			outputData.close();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private Options opts = new Options();
	private CoarseToFineMaxRuleParser parser = null;
	
	public void init(String parserFile) {
		opts.grFileName = parserFile;
//		opts.kbest = 1;
//		opts.binarize = true;
		
		double threshold = 1.0;
		
		String inFileName = opts.grFileName;
		ParserData pData = ParserData.Load(inFileName);
		if (pData == null) {
			System.out.println("Failed to load grammar from file" + inFileName + ".");
			return;
		}
		Grammar grammar = pData.getGrammar();
		Lexicon lexicon = pData.getLexicon();
		Numberer.setNumberers(pData.getNumbs());
		if (opts.kbest == 1) {
			parser = new CoarseToFineMaxRuleParser(grammar, lexicon, threshold, -1, opts.viterbi, opts.substates, opts.scores, opts.accurate, opts.variational, true, true);
		} else {
			parser = new CoarseToFineNBestParser(grammar, lexicon, opts.kbest, threshold, -1, opts.viterbi, opts.substates, opts.scores, opts.accurate, opts.variational, false, true);
		}
		parser.binarization = pData.getBinarization();
	}
	
	@Trace
	@Measure
	public void parseFile(String inputFile) {
		try {
			BufferedReader inputData = (inputFile == null) ? new BufferedReader(
					new InputStreamReader(System.in)) : new BufferedReader(
					new InputStreamReader(new FileInputStream(inputFile),
							"UTF-8"));
			PrintWriter outputData = (opts.outputFile == null) ? new PrintWriter(
					new OutputStreamWriter(System.out)) : new PrintWriter(
					new OutputStreamWriter(
							new FileOutputStream(opts.outputFile), "UTF-8"),
					true);
			

			/*String line = "";
			while ((line = inputData.readLine()) != null) {
				List<Tree<String>> result = parse(line);
				//@TODO: Dateiausgabe
			}*/
			outputData.flush();
			outputData.close();
			inputData.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@Trace
	@Measure
	public List<Tree<String>> parse(String line) {
		line = line.trim();
		List<String> sentence = null;
		List<String> posTags = null;
		
		sentence = Arrays.asList(line.split("\\s+"));
		
		List<Tree<String>> parsedTrees = null;
		if (opts.kbest > 1) {
			parsedTrees = parser.getKBestConstrainedParses(sentence, posTags, opts.kbest);
			if (parsedTrees.size() == 0) {
				parsedTrees.add(new Tree<String>("ROOT"));
			}
		} else {
			parsedTrees = new ArrayList<Tree<String>>();
			Tree<String> parsedTree = parser.getBestConstrainedParse(sentence, posTags,	null);
			if (opts.goldPOS && parsedTree.getChildren().isEmpty()) { // parse error when using goldPOS, try without
				parsedTree = parser.getBestConstrainedParse(sentence, null, null);
			}
			parsedTrees.add(parsedTree);
		}
		return parsedTrees;
	}
}
