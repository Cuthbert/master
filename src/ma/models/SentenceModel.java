package ma.models;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import biz.source_code.base64Coder.Base64Coder;
import edu.berkeley.nlp.syntax.Tree;

public class SentenceModel extends TextModel {
	private int entryId;
	
	private List<AnnotationModel> annotations = new LinkedList<AnnotationModel>();
	private String parsedTree;
	private String serialisedTree;
	private String serialisedCleanedTree;
	private Tree<String> tree = null;
	private Tree<String> lemmatizedTree = null;
	
	public List<AnnotationModel> getAnnotations() {
		return annotations;
	}
	public void setAnnotations(List<AnnotationModel> annotations) {
		this.annotations = annotations;
	}
	public void addAnnotation(AnnotationModel annotationModel) {
		this.annotations.add(annotationModel);
	}
	
	public String getParsedTree() {
		return parsedTree;
	}
	public void setParsedTree(String parsedTree) {
		this.parsedTree = parsedTree;
	}
	
	public String getAnnotationString() {
		try {
			return Base64Coder.toString((LinkedList<AnnotationModel>)annotations);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public void setAnnotations(String serializedAnnotations) {
		if(serializedAnnotations==null || serializedAnnotations.length()==0) return;
		try {
			setAnnotations((LinkedList<AnnotationModel>)Base64Coder.fromString(serializedAnnotations));
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setSerialisedTree(String serializedTree) {
		this.serialisedTree = serializedTree;
	}
	public boolean hasSerialisedTree() {
		return serialisedTree!=null;
	}
	public void setSerialisedCleanedTree(String serialisedCleanedTree) {
		this.serialisedCleanedTree = serialisedCleanedTree;
	}
	public String getSerialisedTree() {
		return serialisedTree;
	}
	public String getSerialisedCleanedTree() {
		return serialisedCleanedTree;
	}
	
	public Tree<String> getTree() {
		return this.getTree(false);
	}
	
	@SuppressWarnings("unchecked")
	public Tree<String> getTree(boolean lemmatized) {
		try {
			//generate tree
			if(this.tree==null) {
				try {
					this.tree = (Tree<String>) Base64Coder.fromString(serialisedCleanedTree);
				} catch(IllegalArgumentException e) {
					try {
						System.out.println(e.getMessage()+"\nTree:"+serialisedCleanedTree+"\nTrying again...");
						this.tree = (Tree<String>) Base64Coder.fromString(serialisedCleanedTree);
					} catch(IllegalArgumentException ee) {
						return new Tree<String>("");
					}
				}
				
			}
			
			//create lemmatized tree 
			if(this.lemmatizedTree==null && annotations!=null && annotations.size()>0) {
				this.lemmatizedTree = (Tree<String>) Base64Coder.fromString(serialisedCleanedTree);
				Map<String, String> lemmata = new HashMap<>();
				for(AnnotationModel annotationModel:annotations) {
					String lemma = annotationModel.getLemma();
					String text = annotationModel.getText();
					if(!annotationModel.getTag().startsWith("$") && text.length()>1 && !lemma.equals("<unknown>")) {
						if(lemma.contains("|")) {
							String[] temp = lemma.split("|");
							lemma = temp[0];
						}
						lemmata.put(text, lemma);
					}
				}
				for(Tree<String> terminal:this.lemmatizedTree.getTerminals()) {
					String label = terminal.getLabel(); 
					if(lemmata.containsKey(label)) {
						terminal.setLabel(lemmata.get(label));
					//maybe a word with a [,;.-?!]? 
					} else if(label.length()>0 && lemmata.containsKey(label.substring(0, label.length()-1))) {
						terminal.setLabel(lemmata.get(label.substring(0, label.length()-1)));
					} else {
						terminal.setLabel("");
					}
				}
			}
			
			return lemmatized?this.lemmatizedTree:this.tree;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public int getEntryId() {
		return entryId;
	}
	public void setEntryId(int entryId) {
		this.entryId = entryId;
	}
	@Override
	public Set<String> getLemmata() {
		HashSet<String> lemmata = new HashSet<>();
		for(AnnotationModel annotation:annotations) {
			lemmata.add(annotation.getLemma());
		}
		return lemmata;
	}
}
