package ma.models;

import java.util.LinkedList;
import java.util.List;

public class ClassificationResultDetail<T extends TextModel> {
	public T text = null;
	public List<Double> kernelValues = new LinkedList<>();
	/**
	 * Contains either f-value or probability (or something else) depending on classifier and its relevant order machanic
	 */
	public double characteristic = 0;
}
