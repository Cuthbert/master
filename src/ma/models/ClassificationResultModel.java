package ma.models;

import java.util.LinkedList;

public class ClassificationResultModel<T extends TextModel> {
	public double tp = 0;
	public double fp = 0;
	public double tn = 0;
	public double fn = 0;
	
	public double minf = 0;
	public double maxf = 0;
	
	public double[] fValues;
	public int[] classifiedClasses;
	public int[] classes;
	public double[] probabilities;
	
	public double auc = -1.0;
		
	public LinkedList<ClassificationResultDetail<T>> uncertainTexts = new LinkedList<ClassificationResultDetail<T>>();
	
	
	public double minimum;
	
	public int lastNumberOfTrainingData = -1;
	
	public ClassificationResultModel(int size) {
		fValues = new double[size];
		classes = new int[size];
		classifiedClasses = new int[size];
		probabilities = new double[size];
	}
	
	public LinkedList<T> getUncertainTexts() {
		LinkedList<T> textModels = new LinkedList<T>();
		for(ClassificationResultDetail<T> detail:uncertainTexts) {
			textModels.add(detail.text);
		}
		return textModels;
	}
	
	public String printResult() {
		String output = "";
		output += "Classification Results:\n";
		output += "f(Min/Max): "+minf+"/"+maxf+"\n";
		output += "TP: "+tp+"\n";
		output += "FP: "+fp+"\n";
		output += "FN: "+fn+"\n";
		output += "TN: "+tn+"\n";
		double accuracy = getAccuracy();
		double recall = getRecall();
		double precision = getPrecision();
		double nprecision = getNprecision();
		double f1 = getF1();
		
		output += "Accuracy = "+accuracy+"\n";
		output += "True positive rate (recall) = "+recall+"\n";
		output += "Positive predictive value (precision) = "+precision+"\n";
		output += "Negative predictive value = "+nprecision+"\n";
		output += "f1 = "+f1+"\n";
		output += "AuC = "+auc;
		return output;
	}

	public double getTp() {
		return tp;
	}

	public double getFp() {
		return fp;
	}

	public double getTn() {
		return tn;
	}

	public double getFn() {
		return fn;
	}

	public double getMinf() {
		return minf;
	}

	public double getMaxf() {
		return maxf;
	}

	public double[] getfValues() {
		return fValues;
	}

	public int[] getClasses() {
		return classes;
	}

	public double getAccuracy() {
		return (tp+tn)*1.0/(tp+fp+fn+tn);
	}

	public double getRecall() {
		return tp*1.0/(tp+fn);
	}

	public double getPrecision() {
		return tp*1.0/(tp+fp);
	}

	public double getNprecision() {
		return tn*1.0/(tn+fn);
	}

	public double getF1() {
		return 2.0*tp/(2*tp+fp+fn);
	}
	
	public LinkedList<ClassificationResultDetail<T>> getUncertainTextDetails() {
		return uncertainTexts;
	}
	
	public String printUncertain() {
		String out = "";
		for(ClassificationResultDetail<T> detail:uncertainTexts) {
			out+=detail.text.getId()+" ("+detail.characteristic+")\n";
		}
		return out;
	}

	public double getAuc() {
		return auc;
	}

	public void setAuc(double auc) {
		this.auc = auc;
	}
}
