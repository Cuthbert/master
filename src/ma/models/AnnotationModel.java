package ma.models;

import java.io.Serializable;

@SuppressWarnings("serial")
public class AnnotationModel implements Serializable {
	private String tag;
	private String lemma;
	private String text;
	
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getLemma() {
		return lemma;
	}
	public void setLemma(String lemma) {
		this.lemma = lemma;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	@Override
	public String toString() {
		return text+" "+tag+" "+lemma;
	}
}
