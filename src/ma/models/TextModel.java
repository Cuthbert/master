package ma.models;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public abstract class TextModel {
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	private String text;
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	private List<String> labels = new LinkedList<String>();
	public List<String> getLabels() {
		return labels;
	}
	public void setLabels(List<String> labels) {
		this.labels = labels;
	}
	
	private String status = "";
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	private String cleanedText;
	public String getCleanedText() {
		return cleanedText;
	}
	public void setCleanedText(String cleanedText) {
		this.cleanedText = cleanedText;
	}
	
	public abstract Set<String> getLemmata();
}
