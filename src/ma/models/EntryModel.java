package ma.models;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class EntryModel extends TextModel{
	private LinkedList<SentenceModel> sentences = new LinkedList<SentenceModel>();
	
	public LinkedList<SentenceModel> getSentences() {
		return sentences;
	}
	public void setSentences(LinkedList<SentenceModel> sentences) {
		this.sentences = sentences;
	}
	
	@Override
	public String getCleanedText() {
		String output = "";
		for(SentenceModel sentenceModel:sentences) {
			output+=sentenceModel.getCleanedText()+" ";
		}
		return output.trim();
	}
	@Override
	public Set<String> getLemmata() {
		HashSet<String> lemmata = new HashSet<>();
		for(SentenceModel sentenceModel:sentences) {
			for(AnnotationModel annotation:sentenceModel.getAnnotations()) {
				lemmata.add(annotation.getLemma());
			}
		}
		return lemmata;
	}
}