package ma.aspects;

import ma.tools.MeasureTimer;

public aspect StopWatch {
	pointcut measuredMethod():
		call(@ma.annotations.Measure * *(..));
	
	before():measuredMethod() {
		MeasureTimer.start(thisJoinPoint.getSignature().toString());
	}
	after():measuredMethod() {
		MeasureTimer.stop(thisJoinPoint.getSignature().toString());
	}
}
