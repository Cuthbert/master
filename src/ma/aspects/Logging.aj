package ma.aspects;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;

public aspect Logging {
	static Logger logger = Logger.getRootLogger();
	
	private void logStart(JoinPoint joinPoint) {
		log("==> ", joinPoint);
	}
	
	private void logEnd(JoinPoint joinPoint) {
		log("<== ",joinPoint);
	}
	
	private void log(String step, JoinPoint joinPoint) {
		logger.info(step+joinPoint.getSignature().getDeclaringType().getSimpleName()+"."+joinPoint.getSignature().getName()+"@"+joinPoint.getSourceLocation());
	}
	
	public static void setLogger(Logger pLogger) {
		logger = pLogger;
	}
	
	pointcut tracedFunctionCalls():
		call(@ma.annotations.Trace * *(..));
		
	before():tracedFunctionCalls() {
		logStart(thisJoinPoint);
	}
	after():tracedFunctionCalls() {
		logEnd(thisJoinPoint);
	}
	
	/*pointcut tracedTestMethod():
		call(int ma.modules.test.Scratchbook.testMethod(..));
	
	before(): tracedTestMethod() {
		logStart(thisJoinPoint);
		System.out.println(thisJoinPoint.getKind());
		System.out.println(thisJoinPoint.getTarget());
		for(int i=0; i<thisJoinPoint.getArgs().length;i++) {
			System.out.println(thisJoinPoint.getArgs()[i]);
		}
		System.out.println(thisJoinPoint.toLongString());
		System.out.println(thisJoinPoint.toShortString());
		System.out.println(thisJoinPoint.toString());
		
		System.out.println(thisJoinPoint.getSignature());
		System.out.println(thisJoinPoint.getSignature().getDeclaringTypeName());
		System.out.println(thisJoinPoint.getSignature().getModifiers());
		System.out.println(thisJoinPoint.getSignature().getName());
//		System.out.println(thisJoinPoint.getSignature().getClass().getName());
		System.out.println("DeclaringType.getName: "+thisJoinPoint.getSignature().toString());
		System.out.println("DeclaringType.getName: "+thisJoinPoint.getSignature().getDeclaringType().getName());
		System.out.println("DeclaringType.getSimpleName: "+thisJoinPoint.getSignature().getDeclaringType().getSimpleName());
		System.out.println(thisJoinPoint.getSignature().getDeclaringType());
		System.out.println(thisJoinPoint.getSourceLocation());
		
	}
	
	after():tracedTestMethod() {
		logEnd(thisJoinPoint);
	}*/
}
