package ma.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ma.annotations.Measure;
import ma.annotations.Trace;
import ma.models.EntryModel;
import ma.models.SentenceModel;
import ma.models.TextModel;

public class ArffDatabaseHandler extends DatabaseHandler {
	public ArffDatabaseHandler(String databasePath, String sqlFile) {
		super(databasePath, sqlFile);
	}
	
	public ArffDatabaseHandler(String databasePath) {
		super(databasePath);
	}
	
	@Override
	public void initialiseDatabase(boolean pruneOldData) {
		try {
			Statement statement = getConnection().createStatement();
			//check for already initialized database
			ResultSet rsCount = statement.executeQuery("select count(*) FROM sqlite_master WHERE type='table';");
			rsCount.next();
			if(rsCount.getInt(1)!=0) {
				logger.warn("Database already initialised.");
				if(pruneOldData) {
					logger.debug("Tables will be cleared.");
					statement.executeUpdate("delete from entry");
					statement.executeUpdate("delete from attribute");
					statement.executeUpdate("delete from sentence");
					statement.executeUpdate("delete from entry_attribute");
//					statement.executeUpdate("delete from kernel_matrix");
				}
			} else {
				statement.executeUpdate("CREATE TABLE entry (id INTEGER PRIMARY KEY, text TEXT, textValue DOUBLE, status TEXT, sentence_count NUMERIC)");
				statement.executeUpdate("CREATE TABLE attribute (id INTEGER PRIMARY KEY, name TEXT, nameIndex INTEGER)");
				/*external id: der Datensatz im ARFF-File, so geht die info nicht verloren, welches Attribut an welchem Beispiel hing, 
				kann über ein distinct wieder rausselected werden*/
				statement.executeUpdate("CREATE TABLE entry_attribute (entry_id INTEGER, attribute_id INTEGER, externalId INTEGER)");
				statement.executeUpdate("CREATE TABLE sentence (id INTEGER PRIMARY KEY, text TEXT, cleaned_text TEXT, pos_tags BLOB, parsed_tree TEXT, serialised_tree BLOB, serialised_cleaned_tree BLOB, entry_id INTEGER, entry_position INTEGER, status TEXT)");
				/*				statement.executeUpdate("CREATE TABLE kernel_matrix (id1 INTEGER, id2 INTEGER, value DOUBLE)");
				statement.executeUpdate("CREATE TABLE sentence_raw (sentence_id INTEGER, text TEXT)");
				statement.executeUpdate("CREATE TABLE sentence_clean (sentence_id INTEGER, text TEXT)");
				statement.executeUpdate("CREATE TABLE annotation (sentence_id INTEGER, start INTEGER, end INTEGER, text TEXT, lemma TEXT, tag TEXT)");
				statement.executeUpdate("CREATE TABLE tree (sentence_id INTEGER, tree TEXT)");*/
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Measure
	public void createAttribute(String text, int nameValue) throws SQLException {
		PreparedStatement statement = getConnection().prepareStatement("insert into attribute (name, nameIndex) values (?,?)");
			statement.setString(1, text);
			statement.setInt(2, nameValue);
			statement.executeUpdate();
			statement.close();
//			getConnection().commit();
	}
	
	/**
	 * Inserts a new entry in the database. If entry already known, just return id
	 * @return id
	 * @throws SQLException 
	 */
	@Measure
	public int createEntry(String text, double textValue) throws SQLException {
		int key = -1;
			PreparedStatement statement = getConnection().prepareStatement("insert into entry (text, textValue) values (?,?)");
			statement.setString(1, text);
			statement.setDouble(2, textValue);
			statement.executeUpdate();
			
			statement = getConnection().prepareStatement("SELECT last_insert_rowid()");
			ResultSet rsId = statement.executeQuery();
			rsId.next();
			key = rsId.getInt(1);
			statement.close();
		return key;
	}
	
	@Measure
	public void connectEntryAndAttribute(int entryKey, int attributeKey, int externalId) throws SQLException {
		PreparedStatement statement = getConnection().prepareStatement("INSERT INTO entry_attribute (entry_id, attribute_id, externalId) values(?,?,?)");
		statement.setInt(1, entryKey);
		statement.setInt(2, attributeKey);
		statement.setInt(3, externalId);
		statement.executeUpdate();
		statement.close();
	}
	
	@Measure
	public int getKeyForTextEntryValue(double textValue) throws SQLException {
		int key=-1;
			PreparedStatement statement = getConnection().prepareStatement("select id from entry where textValue = ?");
			statement.setDouble(1, textValue);
			ResultSet rs = statement.executeQuery();
			if(rs.next()) {
				key = rs.getInt(1);
			}
			statement.close();
		return key;
	}
	
	@Measure
	public String getEntryText(int id) throws SQLException {
		PreparedStatement statement = getConnection().prepareStatement("SELECT text FROM entry where id = ?");
		statement.setInt(1, id);
		ResultSet resultSet = statement.executeQuery();
		if(resultSet.next()) {
			return resultSet.getString(1);
			
		}
		return null;
	}
	
	public void commit() throws SQLException {
		getConnection().commit();
	}

	public void close() throws SQLException {
		commit();
		getConnection().close();
	}

	public HashMap<Integer, String> getEntries() throws SQLException {
		return getEntries(false);
	}
	
	@Trace
	@Measure
	public HashMap<Integer, String> getEntries(boolean unsplittedOnly) throws SQLException {
		HashMap<Integer, String> data = new HashMap<Integer, String>();
		String joinPart = "";
		String wherePart = "";
		if(unsplittedOnly) {
			joinPart = "left join sentence on entry.id=sentence.entry_id";
			wherePart = " where sentence.text is null";
		}
		PreparedStatement statement = getConnection().prepareStatement("SELECT entry.id, entry.text FROM entry "+joinPart+wherePart);
		ResultSet resultSet = statement.executeQuery();
		while(resultSet.next()) {
			data.put(resultSet.getInt(1), resultSet.getString(2));
		}
		statement.close();
		return data;
	}

	@Measure
	public int addSentence(String sentence, String cleanedSentence, int entryKey, int position) throws SQLException {
		PreparedStatement statement = getConnection().prepareStatement("INSERT INTO sentence (text, cleaned_text, entry_id, entry_position, status) VALUES (?,?,?,?,'new')");
		statement.setString(1, sentence);
		statement.setString(2, cleanedSentence);
		statement.setInt(3, entryKey);
		statement.setInt(4, position);
		statement.executeUpdate();
		
		statement = getConnection().prepareStatement("SELECT last_insert_rowid()");
		ResultSet rsId = statement.executeQuery();
		rsId.next();
		int key = rsId.getInt(1);
		statement.close();
		return key;
	}

	@Measure
	public void updateEntry(EntryModel entryModel) throws SQLException {
		PreparedStatement statement = getConnection().prepareStatement("SELECT * from entry where id=?");
		statement.setInt(1, entryModel.getId());
		ResultSet selectResultSet = statement.executeQuery();
		if(!selectResultSet.next()) { //no entry given
			System.err.println("Entry with id "+entryModel.getId()+" does not exist in database.");
			return;
		}
		statement = getConnection().prepareStatement("DELETE FROM sentence WHERE entry_id=?");
		statement.setInt(1, entryModel.getId());
		statement.executeUpdate();
		
		statement = getConnection().prepareStatement("INSERT INTO sentence (text, cleaned_text, parsed_tree, pos_tags, entry_id, entry_position, serialised_tree, status) VALUES (?,?,?,?,?,?,?,'new')");
		for(SentenceModel sentenceModel:entryModel.getSentences()) {
			statement.setString(1, sentenceModel.getText());
			statement.setString(2, sentenceModel.getCleanedText());
			statement.setString(3, sentenceModel.getParsedTree());
			statement.setString(4, sentenceModel.getAnnotationString());
			statement.setInt(5, entryModel.getId());
			statement.setInt(6, entryModel.getSentences().indexOf(sentenceModel));
			statement.setString(7, sentenceModel.getSerialisedTree());
			statement.executeUpdate();
		}
		statement.close();
		
	}

	public List<SentenceModel> getSentences() throws SQLException {
		return getSentences(false);
	}
	
	public List<SentenceModel> getSentences(boolean fetchOnlyTreeless) throws SQLException {
		return getSentences(fetchOnlyTreeless, 0);
	}
	
	public List<SentenceModel> getSentences(boolean fetchOnlyTreeless, int sentenceLimit) throws SQLException {
		return getSentences(fetchOnlyTreeless, sentenceLimit, null);
	}
	
	public List<SentenceModel> getSentences(boolean fetchOnlyTreeless, int sentenceLimit, String[] filterAttributes) throws SQLException {
		return getSentences(fetchOnlyTreeless, sentenceLimit, filterAttributes, null);
	}

	/**
	 * 
	 * @param fetchOnlyTreeless
	 * @param sentenceLimit
	 * @param filterAttributes
	 * @param filterStatus Möglich: 'new', 'valid', 'invalid', bezieht sich auf den cleanedTree
	 * @return
	 * @throws SQLException
	 */
	
	@Measure
	@Trace 
	public List<SentenceModel> getSentences(boolean fetchOnlyTreeless, int sentenceLimit, String[] filterAttributes, String filterStatus) throws SQLException {
		List<SentenceModel> sentences = new LinkedList<>();
		String condition = "";
		ArrayList<String> options = new ArrayList<>();
		if (fetchOnlyTreeless) {
			options.add("parsed_tree is null");
		}
		if(filterStatus!=null) {
			options.add("status = '"+filterStatus+"'");
		}
		if(options.size()>0) {
			condition=" where "+options.get(0);
			for(int i=1; i<options.size();i++) {
				condition+=" and "+options.get(i);
			}
		}
		String limit = sentenceLimit>0?(" limit "+sentenceLimit):"";
		
		PreparedStatement statement = getConnection().prepareStatement("SELECT id, entry_id, text, cleaned_text, parsed_tree, serialised_tree, serialised_cleaned_tree, status, pos_tags from sentence "+condition+" order by entry_id "+limit);
		ResultSet resultSet = statement.executeQuery();
		Set<String> filter = new HashSet<>();
		if(filterAttributes!=null) { 
			for(int i=0; i<filterAttributes.length;i++) filter.add(filterAttributes[i]);
		}
		while(resultSet.next()) {
			SentenceModel model = new SentenceModel();
			model.setId(resultSet.getInt("id"));
			model.setEntryId(resultSet.getInt("entry_id"));
			if(!filter.contains("text"))
				model.setText(resultSet.getString("text"));
			if(!filter.contains("cleanedText"))
				model.setCleanedText(resultSet.getString("cleaned_text"));
			if(!filter.contains("parsedTree"))
				model.setParsedTree(resultSet.getString("parsed_tree"));
			if(!filter.contains("serialisedTree"))
				model.setSerialisedTree(resultSet.getString("serialised_tree"));
			if(!filter.contains("serialisedCleanedTree"))
				model.setSerialisedCleanedTree(resultSet.getString("serialised_cleaned_tree"));
			if(!filter.contains("status"))
				model.setStatus(resultSet.getString("status"));
			if(!filter.contains("pos_tags"))
				model.setAnnotations(resultSet.getString("pos_tags"));
			sentences.add(model);
		}
		statement.close();
		return sentences;
	}
	
	@Measure
	public SentenceModel getSentenceById(Integer entryId) throws SQLException {
		SentenceModel model = null;
		
		PreparedStatement statement = getConnection().prepareStatement("SELECT id, entry_id, text, cleaned_text, parsed_tree, serialised_tree, serialised_cleaned_tree, status, pos_tags from sentence where id=?");
		statement.setInt(1, entryId);
		ResultSet resultSet = statement.executeQuery();
		while(resultSet.next()) {
			model = new SentenceModel();
			model.setId(resultSet.getInt("id"));
			model.setEntryId(resultSet.getInt("entry_id"));
			model.setText(resultSet.getString("text"));
			model.setCleanedText(resultSet.getString("cleaned_text"));
			model.setParsedTree(resultSet.getString("parsed_tree"));
			model.setSerialisedTree(resultSet.getString("serialised_tree"));
			model.setSerialisedCleanedTree(resultSet.getString("serialised_cleaned_tree"));
			model.setAnnotations(resultSet.getString("pos_tags"));
			model.setStatus(resultSet.getString("status"));
		}
		statement.close();
		return model;
	}

	@Measure
	public void updateSentence(SentenceModel sentenceModel) throws SQLException {
		PreparedStatement statement = getConnection().prepareStatement("UPDATE sentence SET parsed_tree = ?, serialised_tree = ?, serialised_cleaned_tree = ?, status = ?, pos_tags = ? WHERE id = ?");
		statement.setString(1, sentenceModel.getParsedTree());
		statement.setString(2, sentenceModel.getSerialisedTree());
		statement.setString(3, sentenceModel.getSerialisedCleanedTree());
		statement.setString(4, sentenceModel.getStatus());
		statement.setString(5, sentenceModel.getAnnotationString());
		statement.setInt(6, sentenceModel.getId());
		statement.executeUpdate();
		statement.close();
	}
	
	@Measure
	public List<String> getLabelsForSentence(Integer sentenceId) throws SQLException {
		List<String> labels = new LinkedList<>();
		PreparedStatement statement = getConnection().prepareStatement("select a.name from sentence s join entry_attribute ea on s.entry_id=ea.entry_id left join attribute a on a.nameIndex=ea.attribute_id where s.id=? ");
		statement.setInt(1, sentenceId);
		ResultSet resultSet = statement.executeQuery();

		while(resultSet.next()) {
			labels.add(resultSet.getString("name"));
		}		
		return labels;
	}
	
	public List<SentenceModel> getUsableSentences(int sentenceLimit, boolean randomSelection, double ratio, String label) throws SQLException {
		return getUsableSentences(sentenceLimit, randomSelection, ratio, label, null);
	}
	
	@Trace
	@Measure
	public List<SentenceModel> getUsableSentences(int sentenceLimit, boolean randomSelection, double ratio, String label, Set<Integer> instanceIds) throws SQLException {
		List<SentenceModel> sentences = new LinkedList<>();
		
		long positive = Math.round(sentenceLimit*ratio);
		String positiveLimit = positive>0?(" limit "+positive):"-";
		long negative = sentenceLimit-positive;
		String negativeLimit = negative>0?(" limit "+negative):"-";
		
		String positiveExclusionQuery = "";
		String negativeExclusionQuery = "";
		if(instanceIds!=null && !instanceIds.isEmpty()) {	
			logger.info("Exclude "+instanceIds.size()+" sentences");
			positiveExclusionQuery=" and s.id not in ("+instanceIds.toString()+")";
			positiveExclusionQuery = positiveExclusionQuery.replaceAll("[\\[\\]]", "");
			negativeExclusionQuery=" and id not in ("+instanceIds.toString()+")";
			negativeExclusionQuery = negativeExclusionQuery.replaceAll("[\\[\\]]", "");
		}
//		System.out.println(exclusionQuery);
		String randomSelectionQuery = "order by random()";
		
		logger.info("Limit+: "+positiveLimit+" Limit-: "+negativeLimit);
		
		
		if(positive>0) {
			PreparedStatement statement = getConnection().prepareStatement("select distinct s.id, s.entry_id, cleaned_text, serialised_cleaned_tree, pos_tags from sentence s join entry_attribute ea on s.entry_id=ea.entry_id left join attribute a on a.nameIndex=ea.attribute_id where serialised_cleaned_tree is not null and a.name=? "+positiveExclusionQuery+" "+(randomSelection?randomSelectionQuery:"")+" "+positiveLimit);
			statement.setString(1, label);
			ResultSet resultSet = statement.executeQuery();
	
			while(resultSet.next()) {
				SentenceModel model = new SentenceModel();
				model.setId(resultSet.getInt("id"));
				model.setEntryId(resultSet.getInt("entry_id"));
				model.setCleanedText(resultSet.getString("cleaned_text"));
				model.setSerialisedCleanedTree(resultSet.getString("serialised_cleaned_tree"));
				model.setAnnotations(resultSet.getString("pos_tags"));
				model.getLabels().add(label);
				sentences.add(model);
			}
		}
		logger.info("Positive: "+sentences.size());
		
		if(negative>0) {
			PreparedStatement statement = getConnection().prepareStatement("SELECT id, entry_id, cleaned_text, serialised_cleaned_tree, pos_tags from sentence where entry_id not in (select distinct s.entry_id from sentence s join entry_attribute ea on s.entry_id=ea.entry_id join attribute a on a.nameIndex=ea.attribute_id and a.name=?) and serialised_cleaned_tree is not null "+negativeExclusionQuery+(randomSelection?randomSelectionQuery:"")+" "+negativeLimit);
			statement.setString(1, label);
			ResultSet resultSet = statement.executeQuery();
	
			while(resultSet.next()) {
				SentenceModel model = new SentenceModel();
				model.setId(resultSet.getInt("id"));
				model.setEntryId(resultSet.getInt("entry_id"));
				model.setCleanedText(resultSet.getString("cleaned_text"));
				model.setSerialisedCleanedTree(resultSet.getString("serialised_cleaned_tree"));
				model.setAnnotations(resultSet.getString("pos_tags"));
				sentences.add(model);
			}
		}
		logger.info("Positive+negative: "+sentences.size());
		Collections.sort(sentences, new Comparator<SentenceModel>() {

			@Override
			public int compare(SentenceModel o1, SentenceModel o2) {
				return o1.getId()>o2.getId()?1:-1;
			}
			
		});
		return sentences;
	}
	
	@Trace
	@Measure
	public Map<String, Integer> getLabelCountsPerSentence(int minimum) throws SQLException {
		Map<String, Integer> labelCounts = new HashMap<String, Integer>();
		String query = minimum>0?sqlConfiguration.getProperty("data.select.attribute_order_limit"):sqlConfiguration.getProperty("data.select.attribute_order");
		PreparedStatement statement = getConnection().prepareStatement(query);
		if(minimum>0) {
			statement.setInt(1, minimum);
		}
		ResultSet resultSet = statement.executeQuery();
		while(resultSet.next()) {
			labelCounts.put(resultSet.getString("name"), resultSet.getInt("namecount"));
		}
		statement = getConnection().prepareStatement(sqlConfiguration.getProperty("data.select.sentence.count"));
		resultSet = statement.executeQuery();
		if(resultSet.next()) {
			labelCounts.put("entry_count", resultSet.getInt("count"));
		}
		return labelCounts;
	}
	
	public Map<String, Integer> getLabelCountsPerSentence() throws SQLException {
		return getLabelCountsPerSentence(-1);
	}
	
	@Trace
	@Measure
	public Map<String, Integer> getLabelCountsPerEntry(int minimum) throws SQLException {
		Map<String, Integer> labelCounts = new HashMap<String, Integer>();
		String query = minimum>0?sqlConfiguration.getProperty("data.select.entry.attribute_order_limit"):sqlConfiguration.getProperty("data.select.entry.attribute_order");
		PreparedStatement statement = getConnection().prepareStatement(query);
		if(minimum>0) {
			statement.setInt(1, minimum);
		}
		ResultSet resultSet = statement.executeQuery();
		while(resultSet.next()) {
			labelCounts.put(resultSet.getString("name"), resultSet.getInt("namecount"));
		}
		statement = getConnection().prepareStatement(sqlConfiguration.getProperty("data.select.entry.count"));
		resultSet = statement.executeQuery();
		if(resultSet.next()) {
			labelCounts.put("entry_count", resultSet.getInt("count"));
		}
		return labelCounts;
	}
	
	public Map<String, Integer> getLabelCountsPerEntry() throws SQLException {
		return getLabelCountsPerEntry(-1);
	}
	
	@Measure
	public EntryModel getEntryById(Integer entryId) throws SQLException {
		EntryModel entryModel = null;
		
		PreparedStatement statement = getConnection().prepareStatement("select e.id as entry_id, s.id as sentence_id from entry e join sentence s on e.id=s.entry_id where e.id = ?");
		statement.setInt(1, entryId);
		ResultSet resultSet = statement.executeQuery();
		while(resultSet.next()) {
			if(entryModel==null) {
				entryModel = new EntryModel();
				entryModel.setId(resultSet.getInt("entry_id"));
				entryModel.setStatus("invalid");
			}
			SentenceModel sentenceModel = this.getSentenceById(resultSet.getInt("sentence_id")); 
			entryModel.getSentences().add(sentenceModel);
			if(sentenceModel.getStatus().equals("valid")) entryModel.setStatus("valid");
		}
		statement.close();
		return entryModel;
	}
	
	@Measure
	public List<String> getLabelsForEntry(Integer entryId) throws SQLException {
		List<String> labels = new LinkedList<>();
		PreparedStatement statement = getConnection().prepareStatement("select a.name from entry_attribute ea left join attribute a on a.nameIndex=ea.attribute_id where ea.entry_id=? ");
		statement.setInt(1, entryId);
		ResultSet resultSet = statement.executeQuery();

		while(resultSet.next()) {
			labels.add(resultSet.getString("name"));
		}		
		return labels;
	}
	
	public List<EntryModel> getUsableEntries(int entryLimit, boolean randomSelection, double ratio, String label) throws SQLException {
		return getUsableEntries(entryLimit, randomSelection, ratio, label, null);
	}
	
	public List<EntryModel> getUsableEntries(int entryLimit, boolean randomSelection, double ratio, String label, Set<Integer> instanceIds) throws SQLException {
		return getUsableEntries(entryLimit, randomSelection, ratio, label, instanceIds, -1);
	}
	
	@Measure
	@Trace
	public List<EntryModel> getUsableEntries(int entryLimit, boolean randomSelection, double ratio, String label, Set<Integer> instanceIds, int sentenceLimit) throws SQLException {
		List<EntryModel> entries = new LinkedList<>();
		
		long positive = Math.round(entryLimit*ratio);
		String positiveLimit = positive>0?(" limit "+positive):"-";
		long negative = entryLimit-positive;
		String negativeLimit = negative>0?(" limit "+negative):"-";
		
		String positiveExclusionQuery = "";
		String negativeExclusionQuery = "";
		if(instanceIds!=null && !instanceIds.isEmpty()) {	
			logger.info("Exclude "+instanceIds.size()+" entries");
			positiveExclusionQuery=" and e.id not in ("+instanceIds.toString()+")";
			positiveExclusionQuery = positiveExclusionQuery.replaceAll("[\\[\\]]", "");
			negativeExclusionQuery=" and id not in ("+instanceIds.toString()+")";
			negativeExclusionQuery = negativeExclusionQuery.replaceAll("[\\[\\]]", "");
		}
		String sentenceLimitQuery = "";
		if(sentenceLimit>0) {
			sentenceLimitQuery = " and sentence_count<="+sentenceLimit;
		}
		String randomSelectionQuery = " order by random()";
		
		logger.info("Limit+: "+positiveLimit+" Limit-: "+negativeLimit);

		if(positive>0) {
			PreparedStatement statement = getConnection().prepareStatement("select s.id, s.entry_id, s.cleaned_text, s.serialised_cleaned_tree, s.pos_tags, s.status from sentence s where s.entry_id in (select e.id from entry e join entry_attribute ea on e.id=ea.entry_id left join attribute a on a.nameIndex=ea.attribute_id where e.status in ('valid','semivalid') and a.name=? "+sentenceLimitQuery+" "+positiveExclusionQuery+" "+(randomSelection?randomSelectionQuery:"")+" "+positiveLimit+") order by s.entry_id, s.id");
			statement.setString(1, label);
			ResultSet resultSet = statement.executeQuery();
	
			int lastEntryId = -1;
			EntryModel currentEntryModel = null;
			while(resultSet.next()) {
				int currentEntryId = resultSet.getInt("entry_id");
				if(currentEntryId!=lastEntryId) {
					if(currentEntryModel!=null) entries.add(currentEntryModel);
					currentEntryModel = new EntryModel();
					currentEntryModel.setId(currentEntryId);
					currentEntryModel.getLabels().add(label);
					lastEntryId=currentEntryId;
				}
				if(resultSet.getString("status").equals("valid")) {
					SentenceModel model = new SentenceModel();
					model.setId(resultSet.getInt("id"));
					model.setEntryId(currentEntryId);
					model.setCleanedText(resultSet.getString("cleaned_text"));
					model.setSerialisedCleanedTree(resultSet.getString("serialised_cleaned_tree"));
					model.setAnnotations(resultSet.getString("pos_tags"));
					model.getLabels().add(label);
					currentEntryModel.getSentences().add(model);
				}
			}
			if(currentEntryModel!=null) entries.add(currentEntryModel);
		}
		logger.info("Positive: "+entries.size());
		
		if(negative>0) {
			PreparedStatement statement = getConnection().prepareStatement("SELECT s.id, s.entry_id, s.cleaned_text, s.serialised_cleaned_tree, s.pos_tags, s.status from sentence s where s.entry_id in (select e.id from entry e where e.id not in (select distinct ea.entry_id from entry_attribute ea join attribute a on a.nameIndex=ea.attribute_id and a.name=?) and e.status in ('valid', 'semivalid') "+sentenceLimitQuery+negativeExclusionQuery+(randomSelection?randomSelectionQuery:"")+" "+negativeLimit+") order by s.entry_id, s.id");
			statement.setString(1, label);
			ResultSet resultSet = statement.executeQuery();
	
			int lastEntryId = -1;
			EntryModel currentEntryModel = null;
			while(resultSet.next()) {
				int currentEntryId = resultSet.getInt("entry_id");
				if(currentEntryId!=lastEntryId) {
					if(currentEntryModel!=null) entries.add(currentEntryModel);
					currentEntryModel = new EntryModel();
					currentEntryModel.setId(currentEntryId);
					lastEntryId=currentEntryId;
				}
				if(resultSet.getString("status").equals("valid")) {
					SentenceModel model = new SentenceModel();
					model.setId(resultSet.getInt("id"));
					model.setEntryId(currentEntryId);
					model.setCleanedText(resultSet.getString("cleaned_text"));
					model.setSerialisedCleanedTree(resultSet.getString("serialised_cleaned_tree"));
					model.setAnnotations(resultSet.getString("pos_tags"));
					currentEntryModel.getSentences().add(model);
				}
			}
			if(currentEntryModel!=null) entries.add(currentEntryModel);
		}
		
		logger.info("Positive+negative: "+entries.size());
		Collections.sort(entries, new Comparator<TextModel>() {

			@Override
			public int compare(TextModel o1, TextModel o2) {
				return o1.getId()>o2.getId()?1:-1;
			}
			
		});
		return entries;
	}
}
