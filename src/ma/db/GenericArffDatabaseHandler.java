package ma.db;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ma.models.EntryModel;
import ma.models.SentenceModel;
import ma.models.TextModel;

public class GenericArffDatabaseHandler<T extends TextModel> extends ArffDatabaseHandler {
	private boolean isSentenceModel = false;
	private boolean isEntryModel = false;
	public GenericArffDatabaseHandler(String databasePath, String sqlFile) {
		super(databasePath, sqlFile);
		// TODO Auto-generated constructor stub
	}
	
	public GenericArffDatabaseHandler(String databasePath) {
		super(databasePath);
		// TODO Auto-generated constructor stub
	}
	
	public void setMode(int mode) {
		isSentenceModel = mode==1;
		isEntryModel = mode==2;
	}

	//GENERIC
	public Map<String, Integer> getLabelCountsPerText() throws SQLException {
		return getLabelCountsPerText(-1);
	}
	public Map<String, Integer> getLabelCountsPerText(int minimum) throws SQLException {
		if(isSentenceModel) {
			return getLabelCountsPerSentence(minimum);
		} else if(isEntryModel) {
			return getLabelCountsPerEntry(minimum);
		} else {
			try {
				throw new Exception("Type-depending database method used and no mode set.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public T getTextById(Integer id) throws SQLException {
		if(isSentenceModel) {
			return (T)getSentenceById(id);
		} else if(isEntryModel) {
			return (T)getEntryById(id);
		} else {
			try {
				throw new Exception("Type-depending database method used and no mode set.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public List<String> getLabelsForText(Integer textId) throws SQLException {
		if(isSentenceModel) {
			return getLabelsForSentence(textId);
		} else if(isEntryModel) {
			return getLabelsForEntry(textId);
		} else {
			try {
				throw new Exception("Type-depending database method used and no mode set.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public List<T> getUsableTexts(int limit, boolean randomSelection, double ratio, String label) throws SQLException {
		return getUsableTexts(limit, randomSelection, ratio, label, null);
	}
	
	public List<T> getUsableTexts(int limit, boolean randomSelection, double ratio, String label, Set<Integer> instanceIds) throws SQLException {
		return getUsableTexts(limit, randomSelection, ratio, label, instanceIds, -1);
	}
	
	@SuppressWarnings("unchecked")
	public List<T> getUsableTexts(int limit, boolean randomSelection, double ratio, String label, Set<Integer> instanceIds, int sentenceLimit) throws SQLException {
		List<T> temp = new LinkedList<T>();
		if(isSentenceModel) {
			List<SentenceModel> t = getUsableSentences(limit, randomSelection, ratio, label, instanceIds);
			for(SentenceModel model:t) {
				temp.add((T)model);
			}
		} else if(isEntryModel) {
			List<EntryModel> t = getUsableEntries(limit, randomSelection, ratio, label, instanceIds, sentenceLimit);
			for(EntryModel model:t) {
				temp.add((T)model);
			}
		} else {
			try {
				throw new Exception("Type-depending database method used and no mode set.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return temp;
	}
}
