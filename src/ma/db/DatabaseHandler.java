package ma.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

abstract public class DatabaseHandler {
	private Connection connection = null;
	private String databaseName = null;
	protected Properties sqlConfiguration = null;
	protected Logger logger = Logger.getLogger(DatabaseHandler.class);
	
	public DatabaseHandler(String database) {
		this(database, null);
	}
	
	public DatabaseHandler(String database, String sqlFile) {
		connection = getDatabase(database);
		databaseName = database;
		sqlConfiguration = new Properties();
		try {
			if(sqlFile!=null) {
				sqlConfiguration.load(new FileReader(sqlFile));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	private Connection getDatabase(String databasePath) {
		Connection connection = null;
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:"+databasePath, "", "");
			connection.setAutoCommit(false);
			
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			
			try {
				if (connection != null && !connection.isClosed()) {
					connection.rollback();
					connection.close();
				}
			} catch (SQLException sql) {
				// ignore
			}
			return null;
		}
		return connection;
	}

	public Connection getConnection() {
		return connection;
	}
	
	public abstract void initialiseDatabase(boolean pruneOldData);

	public String getDatabaseName() {
		return databaseName;
	}
}
