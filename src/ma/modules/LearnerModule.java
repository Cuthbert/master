package ma.modules;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ma.Options;
import ma.annotations.Measure;
import ma.annotations.Trace;
import ma.db.GenericArffDatabaseHandler;
import ma.ml.Probability;
import ma.models.ClassificationResultDetail;
import ma.models.ClassificationResultModel;
import ma.models.TextModel;
import ma.tools.Sorter;
import ma.tools.files.FileHandler;
import ma.tools.files.SimpleFileWriter;
import ma.tools.files.StatisticFileWriter;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import weka.classifiers.functions.SMO;
import weka.classifiers.functions.supportVector.PrecomputedKernelMatrixKernel;
import weka.core.Instances;
import weka.core.Utils;

public class LearnerModule<T extends TextModel> extends BasicModule {
	private Logger learnLogger;
	
	public LearnerModule() {
		learnLogger = Logger.getLogger(LearnerModule.class);
		learnLogger.setAdditivity(false);
		learnLogger.setLevel(Level.ALL);
	}
	
	private ClassificationResultModel<T> classify(List<T> validationSet, Map<Integer, T> knownInstances, Instances instances, HashMap<Integer, Double> weights, double bias, Options options) {
		return classify(validationSet, knownInstances, instances, weights, bias, options, false);
	}
	
	@Trace
	@Measure
	private ClassificationResultModel<T> classify(List<T> validationSet, Map<Integer, T> knownInstances, Instances instances, HashMap<Integer, Double> weights, double bias, Options options, boolean createUncertainList) {
		ClassificationResultModel<T> classificationResult = new ClassificationResultModel<>(validationSet.size());
//		classificationResult.setTag("SVM with Kernel "+options.kernelInstance.getClass().getName());
		classificationResult.minf = Double.MAX_VALUE;
		classificationResult.maxf = Double.MIN_VALUE;
		classificationResult.minimum = Double.POSITIVE_INFINITY;
		
		int iterator = 0;
		List<List<Double>> kernelValues = new LinkedList<>();
		for(T validationText:validationSet) {			
			String outputLine = validationText.getId()+": "; 
			double f = bias;
			//collects values for current validation text
			List<Double> tempKernelValues = new LinkedList<>();
			
			for(int i=0; i<instances.numInstances();i++) {
				Integer instanceId = Integer.parseInt(instances.instance(i).stringValue(0).substring(3));
				T textModel = knownInstances.get(instanceId);

				options.kernelInstance.init(textModel, validationText);
				
				//berechne Kernel-Wert IMMER, aber...
				double kernelValue = options.kernelInstance.compute();
				tempKernelValues.add(kernelValue);
				//betrachte ihn in der Berechnung nur wenn es ein Gewicht dafür gibt
				if(weights.get(instanceId)!=null) {
					f+=kernelValue*weights.get(instanceId)*Double.parseDouble(instances.instance(i).stringValue(1));
				}
			}
			classificationResult.fValues[iterator] = f;
			classificationResult.classifiedClasses[iterator] = f>0?1:-1;
			classificationResult.classes[iterator] = validationText.getLabels().contains(options.label)?1:-1;
			iterator++;
			
			outputLine+=f+"\t"+(f<0?"Nein":"Ja")+"("+(validationText.getLabels().contains(options.label)?"Ja":"Nein")+")";
			if(f<classificationResult.minf) classificationResult.minf=f;
			if(f>classificationResult.maxf) classificationResult.maxf=f;
			if(validationText.getLabels().contains(options.label)) {
				if(f>0) {
					classificationResult.tp++;
				} else {
					classificationResult.fn++;
				}
			} else {
				if(f>0) {
					classificationResult.fp++;
				} else {
					classificationResult.tn++;
				}
			}
			if(createUncertainList)	kernelValues.add(tempKernelValues);
			
			if(options.logLines) learnLogger.info(outputLine);
		}
		//uncertain per sorting; check for memory usage
		if(createUncertainList) {
			double[] absFValues = new double[classificationResult.fValues.length];
			for(int i=0;i<classificationResult.fValues.length;i++) absFValues[i] = Math.abs(classificationResult.fValues[i]);
			int[] sortedIndices = Sorter.treeMapSort(absFValues);
			
			classificationResult.minimum = Math.abs(classificationResult.fValues[sortedIndices[0]]);
			
			int randomElements = (int)Math.round(options.randomRatio*options.noOfNewTrainingInstances);
			for(int i=0; i<options.noOfNewTrainingInstances-randomElements;i++) {
				ClassificationResultDetail<T> detail = new ClassificationResultDetail<>();
				detail.characteristic = classificationResult.fValues[sortedIndices[i]];
				detail.kernelValues = kernelValues.get(sortedIndices[i]);
				detail.text = validationSet.get(sortedIndices[i]);
				classificationResult.uncertainTexts.add(detail);
			}
			int i=0;
			LinkedList<Integer> usedIds = new LinkedList<Integer>();
			while(i<randomElements) {
				int index = (int)Math.round(Math.random()*(sortedIndices.length-1));
				//check if none of the previous choosen
				if(!usedIds.contains(index) && index>=options.noOfNewTrainingInstances-randomElements) {
					ClassificationResultDetail<T> detail = new ClassificationResultDetail<>();
					detail.characteristic = classificationResult.fValues[sortedIndices[index]];
					detail.kernelValues = kernelValues.get(sortedIndices[index]);
					detail.text = validationSet.get(sortedIndices[index]);
					classificationResult.uncertainTexts.add(detail);
					usedIds.add(index);
				} else {
					continue;
				}
				i++;
			}
		}
		classificationResult.probabilities = Probability.plattScaling(classificationResult.fValues, classificationResult.classes);
		return classificationResult;
	}
	

	@Measure
	/**
	 * Returns 'probability'
	 * @param kernelValues
	 * @param testTextKernelValue
	 * @param instanceKernelValues
	 * @param instanceClasses
	 * @param k
	 * @return
	 */
	private double getClassificationByKNN(List<Double> kernelValues, double validationTextKernelValue, double[] instanceKernelValues, boolean[] instanceClasses, int k) {
		double[] distance = new double[kernelValues.size()];
		for(int i=0; i<kernelValues.size(); i++) {
			distance[i] = instanceKernelValues[i]-2*kernelValues.get(i)+validationTextKernelValue;
		}
		int[] sortedDistances = Sorter.treeMapSort(distance);
		int noPos = 0;
		for(int i=0; i<k && i<sortedDistances.length; i++) {
			if(instanceClasses[sortedDistances[i]]) {
				noPos++;
			}
		}
		return noPos*1.0/k;
	}
	
	private ClassificationResultModel<T> classifyKNN(List<T> validationSet, Map<Integer, T> knownInstances, Instances instances, Options options) {
		return classifyKNN(validationSet, knownInstances, instances, options, false);
	}
	
	@Trace
	@Measure
	private ClassificationResultModel<T> classifyKNN(List<T> validationSet, Map<Integer, T> knownInstances, Instances instances, Options options, boolean createUncertainList) {
		ClassificationResultModel<T> classificationResult = new ClassificationResultModel<>(validationSet.size());
//		classificationResult.setTag("kNN with k="+options.k+" and Kernel "+options.kernelInstance.getClass().getName());
		//kernel values for K(i,i) due to kNN; 1. instances, 2. each text object
		double[] instanceKernelValues = new double[instances.numInstances()];
		boolean[] instanceClasses = new boolean[instances.numInstances()];
		double textKernelValue;
		
		//compute instance self kernel values
		for(int i=0; i<instances.numInstances();i++) {
			T textModel = knownInstances.get(Integer.parseInt(instances.instance(i).stringValue(0).substring(3)));
			options.kernelInstance.init(textModel, textModel);
			instanceKernelValues[i] = options.kernelInstance.compute();
			instanceClasses[i] = instances.instance(i).stringValue(1).equals("1");
		}
		
		int iterator = 0;
		for(T textData:validationSet) {
			options.kernelInstance.init(textData, textData);
			textKernelValue = options.kernelInstance.compute();
			List<Double> tempKernelValues = new LinkedList<>();
						
			for(int i=0; i<instances.numInstances();i++) {
				Integer instanceId = Integer.parseInt(instances.instance(i).stringValue(0).substring(3));
				T textModel = knownInstances.get(instanceId);
				options.kernelInstance.init(textModel, textData);
				tempKernelValues.add(options.kernelInstance.compute());
			}
			classificationResult.classes[iterator] = textData.getLabels().contains(options.label)?1:-1;

			double probability = getClassificationByKNN(tempKernelValues, textKernelValue, instanceKernelValues, instanceClasses, options.k);
			classificationResult.probabilities[iterator] = probability;
			classificationResult.classifiedClasses[iterator] = probability>options.kNNThreshold?1:-1;
			
			
			if(textData.getLabels().contains(options.label)) {
				if(classificationResult.classifiedClasses[iterator]==1) {
					classificationResult.tp++;
				} else {
					classificationResult.fn++;
				}
			} else {
				if(classificationResult.classifiedClasses[iterator]==1) {
					classificationResult.fp++;
				} else {
					classificationResult.tn++;
				}
			}
			iterator++;
		}
		
		//uncertain per sorting; check for memory usage
		if(createUncertainList) {
			double[] absProbs = new double[classificationResult.probabilities.length];
			for(int i=0; i<classificationResult.probabilities.length; i++) {
				absProbs[i] = Math.abs(classificationResult.probabilities[i]-options.kNNThreshold);
			}
			int[] sortedIndices = Sorter.treeMapSort(absProbs);
			
			classificationResult.minimum = Math.abs(absProbs[sortedIndices[0]]);
			
			int randomElements = (int)Math.round(options.randomRatio*options.noOfNewTrainingInstances);
			for(int i=0; i<options.noOfNewTrainingInstances-randomElements;i++) {
				ClassificationResultDetail<T> detail = new ClassificationResultDetail<>();
				detail.characteristic = classificationResult.probabilities[sortedIndices[i]];
				detail.text = validationSet.get(sortedIndices[i]);
				classificationResult.uncertainTexts.add(detail);
			}
			int i=0;
			LinkedList<Integer> usedIds = new LinkedList<Integer>();
			while(i<randomElements) {
				int index = (int)Math.round(Math.random()*(sortedIndices.length-1));
				//check if none of the previous choosen
				if(!usedIds.contains(index) && index>=options.noOfNewTrainingInstances-randomElements) {
					ClassificationResultDetail<T> detail = new ClassificationResultDetail<>();
					detail.characteristic = classificationResult.probabilities[sortedIndices[index]];
					detail.text = validationSet.get(sortedIndices[index]);
					classificationResult.uncertainTexts.add(detail);
					usedIds.add(index);
				} else {
					continue;
				}
				i++;
			}
		}
		
		return classificationResult;
	}
	
	private void writeToLogbook(Options options, Date date) {
		/**
		 * Logbook for finding log directory depending on used parameters
		 */
		File logBook = new File("logbook.log");
		try {
			if(!logBook.exists()) {
				logBook.createNewFile();
			}
			BufferedWriter logbookWriter = new BufferedWriter(new FileWriter(logBook, true));
			logbookWriter.write((new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(date)+": "+options.label+"; Kernel "+options.kernel+"("+options.kernelInstance.getClass().getName()+"); Ratio: "+options.ratio+"; Lambda: "+options.lambda+"; # expansion: "+options.expansionSize+"\n");
			logbookWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Trace
	@Measure
	public ClassificationResultModel<T> learn(Options options, LearnerData learnerData) {
		/**
		 * Create log directory
		 */
		File outputDirectory = new File(learnerData.outputDirectory.getPath()+"/"+learnerData.configIndex);
		outputDirectory.mkdir();
	
		
		SimpleFileWriter trainingRatioWriter = new SimpleFileWriter(new File(outputDirectory.getPath()+"/ratio.csv"));
		SimpleFileWriter calculationsWriter = new SimpleFileWriter(new File(outputDirectory.getPath()+"/kernelCalculations.csv"));
		/**
		 * create set new appender
		 */
		PatternLayout layout = new PatternLayout("%d{ISO8601} %-5p [%t] %c(%M): %m%n");
		try {
			learnLogger.removeAllAppenders();
			learnLogger.addAppender(new FileAppender(layout, outputDirectory.getPath()+"/logfile.log"));
		} catch (IOException e3) {
			e3.printStackTrace();
		}
		learnLogger.info("time: "+learnerData.executionDate.toString());
		learnLogger.info(options.values());
				
		/**
		 * initialize log file for iterations
		 */
		StatisticFileWriter statisticFile = new StatisticFileWriter(outputDirectory);
		
		/**
		 * Create entry in log book with key options
		 */
		writeToLogbook(options, learnerData.executionDate);
		/**
		 * Initialize database
		 */
		GenericArffDatabaseHandler<T> handler = new GenericArffDatabaseHandler<>(options.databasePath, "data/sql.cfg");
		handler.setMode(options.kernelInstance.getKernelDataType());
		handler.setLogger(learnLogger);
		
		/**
		 * File prefix for used matrix file
		 */
		String matrixPrefix = options.normalize?"normalized":"values";
		
		/**
		 * Setting the ratio. If not given from config calculate from database 
		 */
		//first for test data
		double testRatio = 1.0;
		Map<String, Integer> labelCount = new HashMap<String, Integer>();
		try {
			labelCount = handler.getLabelCountsPerText();
		} catch (SQLException e2) {
			e2.printStackTrace();
		}
		if(labelCount.containsKey(options.label) && labelCount.containsKey("entry_count")) {
			testRatio = labelCount.get(options.label)*1.0/labelCount.get("entry_count");
			learnLogger.info("Test ratio set to: "+testRatio);
		} else {
			learnLogger.error("No ratio value available for "+options.label);
			statisticFile.close();
			return null;
		}
		
		//if necessary set for training data
		if(Double.isNaN(options.ratio) || options.ratio>=1.0) {
			options.ratio = testRatio;
			learnLogger.info("Training ratio set to: "+options.ratio);
		}
		
		if(options.kNNThreshold>1) {
			options.kNNThreshold = testRatio;
			learnLogger.info("kNN threshold set to: "+options.ratio);
		}

		//Create testset
		List<T> testSet = null;
		List<T> validationSet = null;
		Map<Integer, T> knownInstances = new HashMap<Integer, T>();
		
		try {
			if(options.persistantTestData && learnerData.t != null) {
				testSet = (List<T>) learnerData.t;
			} else {
				testSet = handler.getUsableTexts(options.noOfTestinstances, true, testRatio, options.label, null, options.sentenceLimit);
			}
			if(options.persistantTestData) {
				learnerData.t = (List<TextModel>) testSet;
			}

			/**
			 * Saving the test set
			 */
			FileHandler.createArffFile(outputDirectory, "testset", testSet, options.label);
			if(options.createAlternative) {
				FileHandler.createNDimensionalArffFile(testSet, 10, "testset", outputDirectory, options.label);
			}
			for(T model:testSet) {
				knownInstances.put(model.getId(), model);
			}
			
			//Create validation set
			validationSet = handler.getUsableTexts(options.noOfValidationInstances, true, testRatio, options.label, knownInstances.keySet(), options.sentenceLimit);
			FileHandler.createArffFile(outputDirectory, "validationset", validationSet, options.label);
			if(options.createAlternative) {
				FileHandler.createNDimensionalArffFile(validationSet, 10, "validationset", outputDirectory, options.label);
			}
			for(T model:validationSet) {
				knownInstances.put(model.getId(), model);
			}
			
		} catch (SQLException e2) {
			e2.printStackTrace();
			statisticFile.close();
			return null;
		}
		
		//Get the start training data
		if(options.inputFile==null) {
			exportKernelMatrix(options, knownInstances.keySet(), outputDirectory);
		}
 		

 		double lastAccuracy = 0;
 		double lastAuc = 0;
 		int smallerAccuracyCounter = 0;
 		int smallerAucCounter = 0;
 		
 		NumberFormat format = new DecimalFormat("000");

 		CircularFifoQueue<Double> aucQueue = new CircularFifoQueue<Double>(options.smallerRatioLimit);
 		CircularFifoQueue<Double> accQueue = new CircularFifoQueue<Double>(options.smallerRatioLimit);
 		
 		Instances instances = null;
 		double bias = 0;
 		HashMap<Integer, Double> weights = null;
 		
 		
 		int iterationCount = 0;
 		
 		while(iterationCount<=options.iterations) {
 			iterationCount++;
 			learnLogger.info("ITERATION "+iterationCount+":");
 			
 			List<T> trainSet = new LinkedList<>();
 			
 			try {
 				//reading training data
 				instances = new Instances(new BufferedReader(new FileReader(outputDirectory.getPath()+"/temp_data.arff")));
 				instances.setClassIndex(1);
 				learnLogger.info("Reading "+instances.numInstances()+" instances");
 				int positivesExamples = 0;
 				for(int i=0; i<instances.numInstances();i++) {
 					Integer textId = Integer.parseInt(instances.instance(i).stringValue(0).substring(3));
 					try {		
						if(!knownInstances.containsKey(textId)) {
							T tempModel = handler.getTextById(textId);
							tempModel.setLabels(handler.getLabelsForText(textId));
							knownInstances.put(textId, tempModel);
						}
					} catch (SQLException e) {
						e.printStackTrace();
						continue;
					}
 					
 					//checking ratio from training set
 					if(knownInstances.get(textId).getLabels().contains(options.label)) {
 						positivesExamples++;
 					}
 					trainSet.add(knownInstances.get(textId));
 				}
 				learnLogger.info("Current train data ratio: "+(1.0*positivesExamples/instances.numInstances()));
 				trainingRatioWriter.writeValue((1.0*positivesExamples/instances.numInstances()));
 			} catch (IOException e1) {
 				e1.printStackTrace();
 				break;
 			}
 			
 			if(options.createAlternative) {
				FileHandler.createNDimensionalArffFile(trainSet, 10, "trainset_"+iterationCount, outputDirectory, options.label);
			}
 			
 			learnLogger.info("Known instances: "+knownInstances.size());
 			
 			bias = 0;
 			int counterSVs = 0;
 			weights = new HashMap<Integer, Double>();
 			if(options.classificator.equals(Options.SVM)) {
 				PrecomputedKernelMatrixKernel kernel = new PrecomputedKernelMatrixKernel();
 	 			try {
 	 				learnLogger.info("Setting up kernel");
 	 				String[] wekaOptions = {"-D", "-M", outputDirectory.getPath()+"/temp_"+matrixPrefix+".matrix"};
 	 				kernel.setOptions(wekaOptions);//Utils.splitOptions("-D -M \""+outputDirectory.getPath()+"/temp_"+matrixPrefix+".matrix\""));
 	 			} catch (Exception e) {
 	 				e.printStackTrace();
 	 				break;
 	 			}
 	 			learnLogger.debug("Create SMO");
 	 			SMO smo = new SMO();
 	 			String[] smoOptions = {"-K", "weka.classifiers.functions.supportVector.PrecomputedKernelMatrixKernel", "-C", Double.toString(options.svm_c)};
 	 			try {
 	 				learnLogger.debug("Set options");
 					smo.setOptions(smoOptions);
 					learnLogger.debug("Set kernel");
 					smo.setKernel(kernel);
 					learnLogger.debug("BuildClassifier with C="+smo.getC());
 					smo.buildClassifier(instances);
 				} catch (Exception e1) {
 					e1.printStackTrace();
 				}
 				
 				//Getting values
 	 			learnLogger.debug("Get weights for support vectors");
 				String smoOutput = smo.toString();
 				String[] smoLines = smoOutput.split("\\n");
 				
 				Pattern pattern = Pattern.compile("([+-]?)\\s*([01](\\.[0-9]{1,4})?)\\s*\\*\\s*<(row[0-9]+)");
 				
 				for(int i=0; i<smoLines.length; i++) {
 					Matcher m = pattern.matcher(smoLines[i]);
 					if(m.find()) {
 						counterSVs++;
 						weights.put(Integer.parseInt(m.group(4).substring(3)), Double.parseDouble(m.group(2)));
 					}
 				}
 				learnLogger.info("Support vectors:"+counterSVs+", Weights: "+weights.size());
 				
 				double[][] biasTable = smo.bias();
 				
 				for(int x=0; x<biasTable.length;x++) {
 					for(int y=0; y<biasTable[x].length;y++) {
 						if(biasTable[x][y]!=0) {
 							bias = biasTable[x][y];
 						}
 					}
 				}
 				learnLogger.info("Bias: "+bias);
 				learnLogger.info("Classify TRAIN-SET:");
 				
 				/*
 				 * Classify the train data			
 				 */
 				ClassificationResultModel<T> trainClassificationResult = classify(trainSet, knownInstances, instances, weights, bias, options);	
 				String trainFileName = outputDirectory.getPath()+"/trainset_"+format.format(iterationCount)+"_roc"; 
 				double aucTrain = FileHandler.createRoCCurveandDistribution(trainClassificationResult.probabilities, trainClassificationResult.classes, new File(trainFileName+".png"));
 				trainClassificationResult.setAuc(aucTrain);
 				learnLogger.info(trainClassificationResult.printResult());
 			}
/*
TEST THE Validation-SET
*/
 			ClassificationResultModel<T> validationResultModel = null;
			learnLogger.info("Classify validation-SET:");
			String classValidationFileName = outputDirectory.getPath()+"/validationSet_"+format.format(iterationCount)+"_class";
			String validationFileName = outputDirectory.getPath()+"/validationSet_"+format.format(iterationCount);
			int noOfKernelOperations = 0;
			if(options.classificator.equals(Options.SVM)) {
				ClassificationResultModel<T> validationClassificationResult = classify(validationSet, knownInstances, instances, weights, bias, options);
				noOfKernelOperations = validationSet.size()*counterSVs;
				FileHandler.createProbabilityDistribution(validationClassificationResult.fValues, validationClassificationResult.probabilities, new File(outputDirectory.getPath()+"/validationSet_"+format.format(iterationCount)+"_distr.png")); 
//				FileHandler.exportClassificationResults(testClassificationResult, new File(classTestFileName+"_svm.dat"));
				validationResultModel = validationClassificationResult;
			} else if(options.classificator.equals(Options.KNN)) {
				ClassificationResultModel<T> knnValidationClassificationResult = classifyKNN(validationSet, knownInstances, instances, options);
//				FileHandler.exportClassificationResults(knntestClassificationResult, new File(classTestFileName+"_knn_"+options.k+".dat"));
				validationResultModel = knnValidationClassificationResult;
			}
			learnLogger.info("Create RoC");
			double aucValidation = FileHandler.createRoCCurveandDistribution(validationResultModel.probabilities, validationResultModel.classes, new File(validationFileName+"_roc"+".png"));
			validationResultModel.setAuc(aucValidation);
			learnLogger.info(validationResultModel.printResult());
			learnLogger.info("Create PR");
			FileHandler.createPRCurve(validationResultModel.probabilities, validationResultModel.classes, new File(validationFileName+"_pr"+".png"));

			double accuracy = validationResultModel.getAccuracy();
			
			if(aucValidation<=lastAuc) smallerAucCounter++; else smallerAucCounter=0;
			if(accuracy<=lastAccuracy) smallerAccuracyCounter++; else smallerAccuracyCounter=0;
			learnLogger.info("Last accuracy = "+lastAccuracy);
			learnLogger.info("#smaller ratios in a row = "+smallerAccuracyCounter);
			lastAccuracy = accuracy;
			lastAuc = aucValidation;
			
			aucQueue.add(aucValidation);
			accQueue.add(accuracy);
			
			double aucMin = 1;
			double aucMax = 0;
			for(int i=0; i<aucQueue.size();i++) {
				double tAuc = aucQueue.get(i);
				aucMin = (aucMin>tAuc)?tAuc:aucMin;
				aucMax = (aucMax<tAuc)?tAuc:aucMax;
			}
			double aucDist = aucMax-aucMin;
			
			double accMin = 1;
			double accMax = 0;
			for(int i=0; i<accQueue.size();i++) {
				double tAcc = accQueue.get(i);
				accMin = (accMin>tAcc)?tAcc:accMin;
				accMax = (accMax<tAcc)?tAcc:accMax;
			}
			double accDist = accMax-accMin;

			learnLogger.info("Last accuracies = "+accQueue.toString());
			learnLogger.info("distance = "+(accDist)+" <> "+options.epsilon);
			learnLogger.info("Last AuCs = "+aucQueue.toString());
			learnLogger.info("distance = "+(aucDist)+" <> "+options.epsilon);
			
			statisticFile.write(validationResultModel, iterationCount, counterSVs, bias);
			
			if(options.isSimpleSVM) {
				learnLogger.info("Run once only due to simple svm.");
				break;
			}
			
			if(accuracy>options.threshold) {
				learnLogger.info("Threshold reached.");
				break;
			}
			
			if(iterationCount>options.iterations) {
				learnLogger.info("Maximal number of iterations reached.");
				break;
			}
			if(aucQueue.size()==options.smallerRatioLimit && accQueue.size()==options.smallerRatioLimit && 
					(aucDist<options.epsilon/* || accDist<options.epsilon*/)) {
				learnLogger.info("No change in Accuracy or AuC");
				break;
			}
			
			if((options.smallerRatioLimit>0 && smallerAccuracyCounter>options.smallerRatioLimit) ||
				(options.smallerRatioLimit>0 && smallerAucCounter>options.smallerRatioLimit)) {
				learnLogger.info("Smaller Accuracy in a row: "+smallerAccuracyCounter);
				learnLogger.info("Smaller AUC in a row: "+smallerAucCounter);
				break;
			}
			
/*
 * FOUND ONE OR MORE NEW TEXT FOR EXPANDING THE MODEL
 */		
			learnLogger.info("EXPANDING THE SVM:");
			List<T> expansionTexts;
			try {
				expansionTexts = handler.getUsableTexts(options.expansionSize, true, options.ratio, options.label, knownInstances.keySet(), options.sentenceLimit);
			} catch (SQLException e) {
				e.printStackTrace();
				break;
			}
			learnLogger.info("Classify EXPANSION-SET:");
			ClassificationResultModel<T> expansionClassificationResult = null;
			if(options.classificator.equals(Options.SVM)) {
				expansionClassificationResult = classify(expansionTexts, knownInstances, instances, weights, bias, options, true);
				noOfKernelOperations+=expansionTexts.size()*instances.numInstances();
			} else if(options.classificator.equals(Options.KNN)) {
				expansionClassificationResult = classifyKNN(expansionTexts, knownInstances, instances, options, true);
			}
//			double[] expansionProbabilities = Probability.plattScaling(expansionClassificationResult.fValues, expansionClassificationResult.classes);
//			String expansionFileName = outputDirectory.getPath()+"/roc_expansionset_"+format.format(iterationCount); 
//			FileHandler.createRoCCurveandDistribution(expansionClassificationResult.fValues, expansionProbabilities, expansionClassificationResult.classes, new File(expansionFileName+".png"));
//			expansionClassificationResult.printResult();		
			learnLogger.info("Uncertains: "+expansionClassificationResult.minimum+" ("+expansionClassificationResult.printUncertain()+")");
			
			//write new arff file
			BufferedReader reader;
			try {
				//
				//Write data file
				//
				//create list with expansion data
				LinkedList<T> newData = new LinkedList<>();
				newData.addAll(trainSet);
				newData.addAll(expansionClassificationResult.getUncertainTexts());
				if(options.processLogging) {
					//create copy
					FileHandler.createArffFile(outputDirectory, "trainset_"+format.format(iterationCount-1), trainSet, options.label);
				}
				//overwrite existing
				FileHandler.createArffFile(outputDirectory, "temp", newData, options.label);
					
				
				//
				//Write new kernel file
				//
				if(options.classificator.equals(Options.SVM)) {
					int numInstances = instances.numInstances();
					double[][] newValueMatrix = new double[numInstances+expansionClassificationResult.uncertainTexts.size()][numInstances+expansionClassificationResult.uncertainTexts.size()];
								
					File sourceValuesMatrix = new File(outputDirectory.getPath()+"/temp_values.matrix");
					
	//				source = ((PrecomputedKernelMatrixKernel)smo.getKernel()).getKernelMatrixFile();
	
					reader = new BufferedReader(new FileReader(sourceValuesMatrix));
					reader.readLine();
					
	//				logger.info("Values for the most uncertain text: "+expansionClassificationResult.textDetails.size());
					for(int i=0;i<numInstances;i++) {
						String[] values = reader.readLine().split("\\s");
						//adding existing values
						for(int j=0;j<numInstances;j++) {
							newValueMatrix[i][j] = Double.parseDouble(values[j]);
						}
						//Add new value for already existent data
						for(int j=0;j<expansionClassificationResult.uncertainTexts.size();j++) {
							newValueMatrix[i][numInstances+j] = expansionClassificationResult.uncertainTexts.get(j).kernelValues.get(i);
							newValueMatrix[numInstances+j][i] = newValueMatrix[i][numInstances+j];
						}
					}
					//adding data for new instances
					//first: them with themselves (useful for normalization)
					for(int j=0;j<expansionClassificationResult.uncertainTexts.size();j++) {
						options.kernelInstance.init(expansionClassificationResult.uncertainTexts.get(j).text, expansionClassificationResult.uncertainTexts.get(j).text);
						newValueMatrix[numInstances+j][numInstances+j] = options.kernelInstance.compute();
					}
					noOfKernelOperations+=expansionClassificationResult.uncertainTexts.size();
					
					//second: the rest
					for(int j=0;j<expansionClassificationResult.uncertainTexts.size();j++) {
						for(int i=j+1;i<expansionClassificationResult.uncertainTexts.size();i++) {
							options.kernelInstance.init(expansionClassificationResult.uncertainTexts.get(i).text, expansionClassificationResult.uncertainTexts.get(j).text);
							newValueMatrix[numInstances+i][numInstances+j] = options.kernelInstance.compute();
							newValueMatrix[numInstances+j][numInstances+i] = newValueMatrix[numInstances+i][numInstances+j];
						}
					}
					noOfKernelOperations+=expansionClassificationResult.uncertainTexts.size()*expansionClassificationResult.uncertainTexts.size()/2;
	
					reader.close();
					
					String sourceValuesMatrixPath = sourceValuesMatrix.getAbsolutePath();
					if(options.processLogging) {
						File copyDestinationValuesMatrix = new File(outputDirectory.getPath()+"/trainset_"+format.format(iterationCount-1)+"_values.matrix");
						sourceValuesMatrix.renameTo(copyDestinationValuesMatrix);
					}
					
					FileHandler.createPrecomputedKernelMatrixFile(sourceValuesMatrixPath, newValueMatrix);
					
					
					File sourceNormalizedMatrix = new File(outputDirectory.getPath()+"/temp_normalized.matrix");
					double[][] newNormalizedMatrix = new double[numInstances+expansionClassificationResult.uncertainTexts.size()][numInstances+expansionClassificationResult.uncertainTexts.size()];
					for(int j=0;j<newValueMatrix.length;j++) {
						for(int i=j; i<newValueMatrix.length;i++) {
							double temp = newValueMatrix[i][i]*newValueMatrix[j][j];
							newNormalizedMatrix[i][j] = temp>0?newValueMatrix[i][j]/(Math.sqrt(temp)):0;
							newNormalizedMatrix[j][i] = newNormalizedMatrix[i][j];
						}
					}
					String sourceNormalizedMatrixPath = sourceNormalizedMatrix.getAbsolutePath();
					if(options.processLogging) {
						File copyDestinationNormalizedMatrix = new File(outputDirectory.getPath()+"/trainset_"+format.format(iterationCount-1)+"_normalized.matrix");
						sourceNormalizedMatrix.renameTo(copyDestinationNormalizedMatrix);
					}
					FileHandler.createPrecomputedKernelMatrixFile(sourceNormalizedMatrixPath, newNormalizedMatrix);
				}
				
				Double[] temp = {new Double(instances.numInstances()), new Double(noOfKernelOperations)};
				calculationsWriter.writeValues(temp);
			} catch (IOException e) {
				e.printStackTrace();
			}
 		}
 		
		FileHandler.moveFile(outputDirectory.getPath()+"/temp_data.arff", outputDirectory.getPath()+"/trainset_"+format.format(iterationCount-1)+"_data.arff");
		if(options.processLogging && options.classificator.equals(Options.SVM)) {
			FileHandler.moveFile(outputDirectory.getPath()+"/temp_values.matrix", outputDirectory.getPath()+"/trainset_"+format.format(iterationCount-1)+"_values.matrix");
			FileHandler.moveFile(outputDirectory.getPath()+"/temp_normalized.matrix", outputDirectory.getPath()+"/trainset_"+format.format(iterationCount-1)+"_normalized.matrix");
		}
		statisticFile.close();
		trainingRatioWriter.close();
		calculationsWriter.close();
		
		/*
		TEST THE Test-SET
		*/
		ClassificationResultModel<T> resultModel = null;
		learnLogger.info("Classify Test-SET:");
		String classTestFileName = outputDirectory.getPath()+"/testSet_class";
		String testFileName = outputDirectory.getPath()+"/testSet_";
		if(options.classificator.equals(Options.SVM)) {
			ClassificationResultModel<T> testClassificationResult = classify(testSet, knownInstances, instances, weights, bias, options);
			FileHandler.createProbabilityDistribution(testClassificationResult.fValues, testClassificationResult.probabilities, new File(testFileName+"_distr.png")); 
			resultModel = testClassificationResult;
		} else if(options.classificator.equals(Options.KNN)) {
			ClassificationResultModel<T> knnTestClassificationResult = classifyKNN(testSet, knownInstances, instances, options);
			resultModel = knnTestClassificationResult;
		} else {
			learnLogger.error("Something went wrong.\nStatus:\nClassificator: "+options.classificator+"\nbias: "+bias+"\nweights: "+(weights!=null?"vorhanden":"fehlen")+"\ninstances: "+(instances!=null?"vorhanden":"fehlen"));
		}
		learnLogger.info("Create RoC");
		double aucValidation = FileHandler.createRoCCurveandDistribution(resultModel.probabilities, resultModel.classes, new File(testFileName+"_roc"+".png"));
		resultModel.setAuc(aucValidation);
		resultModel.lastNumberOfTrainingData = instances.numInstances();
		learnLogger.info(resultModel.printResult());
		learnLogger.info("Create PR");
		FileHandler.createPRCurve(resultModel.probabilities, resultModel.classes, new File(testFileName+"_pr"+".png"));
		
		//Wait for logs to be written (http://stackoverflow.com/questions/24215759/log4j2-asynclogger-not-logging-full-data)
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return resultModel;
	}
	
	@Trace
	@Measure
	public void exportKernelMatrix(Options options, Set<Integer> exclusionSet, File directory) {
		GenericArffDatabaseHandler<T> databaseHandler = new GenericArffDatabaseHandler<T>(options.databasePath, "data/sql.cfg");
		databaseHandler.setMode(options.kernelInstance.getKernelDataType());
		databaseHandler.setLogger(learnLogger);
		if(Double.isNaN(options.ratio)) {
			learnLogger.error("Invalid ratio value. "+options.ratio);
			return;
		}
		
		double[][] matrix = new double[options.noOfTrainingInstances][];
		for(int i=0; i<options.noOfTrainingInstances;i++) {
			matrix[i] = new double[options.noOfTrainingInstances];
		}
		
		try {			
			List<T> texts = databaseHandler.getUsableTexts(options.noOfTrainingInstances, options.randomSelection, options.ratio, options.label, exclusionSet, options.sentenceLimit);
			databaseHandler.close();
			
			learnLogger.info("Number of elements fetched: "+texts.size());
			double min=Double.MAX_VALUE;
			double max=Double.MIN_VALUE;
			int count =0;

			FileHandler.createArffFile(directory,options.outputFile, texts, options.label);
			
			for(int i=0; i<texts.size(); i++) {
				T sentence1 = texts.get(i);
				//Calculate same entries for normalization
				for(int j=i; j<texts.size(); j++) {
					count++;
					T sentence2 = texts.get(j);
					options.kernelInstance.init(sentence1, sentence2);
					double value = options.kernelInstance.compute();					
					
					matrix[i][j] = value;
					if (i!=j) matrix[j][i] = value;

					if(count%options.outputInterval==0) {
						learnLogger.info(count +"("+i+","+j+") "+"["+min+"-"+max+"]");
					}
					if(count%options.statsInterval==0) {
//						printMeasures();
					}
				}
			}
									
			FileHandler.createPrecomputedKernelMatrixFile(directory.getPath()+"/"+options.outputFile+"_values.matrix", matrix);
			double[][] normalizedMatrix = new double[matrix.length][matrix.length];
			for(int i=0; i<options.noOfTrainingInstances;i++) {
				for(int j=0; j<options.noOfTrainingInstances;j++) {
					if(matrix[i][i]>0 && matrix[j][j]>0) {
						normalizedMatrix[i][j] = matrix[i][j]/(Math.sqrt(matrix[i][i]*matrix[j][j]));
					} else {
						normalizedMatrix[i][j] = 0;
					}
				}
			}
			
			DecimalFormat df = (DecimalFormat)NumberFormat.getInstance(Locale.ENGLISH);
			df.applyPattern("0.00000000");
			FileHandler.createPrecomputedKernelMatrixFile(directory.getPath()+"/"+options.outputFile+"_normalized.matrix", normalizedMatrix, df);		
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}