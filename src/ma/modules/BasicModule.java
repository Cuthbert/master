package ma.modules;

import ma.tools.MeasureCollector;

public class BasicModule {
	public static void printMeasures() {
		System.out.println("Code\tmean\tmedian\tsum\t#entries");
		for(String code:MeasureCollector.getMeasureCodes()) {
			System.out.println(code+"\t"+MeasureCollector.getMean(code)+"\t"+MeasureCollector.getMedian(code)+"\t"+MeasureCollector.getSum(code)+"\t"+MeasureCollector.getSize(code));
		}
	} 
}
