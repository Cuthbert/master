package ma.modules;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;

import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;

import ma.db.ArffDatabaseHandler;

public class Sentiment {
	public static void main(String args[]) {
//		createArffFile();
		
		String sourceFileName = "sentiment.arff";
		BufferedReader reader;
		// init source file
		File sourceFile = new File(sourceFileName);
		
		ArffReader arff;
		try {
			reader = new BufferedReader(new FileReader(sourceFile));
			arff = new ArffReader(reader);
		} catch (IOException e) {
			System.err.println("Error while opening input file ("+sourceFileName+")");
			e.printStackTrace();
			return;
		}
		Instances data = arff.getData();
		
		//init database
		ArffDatabaseHandler databaseHandler = new ArffDatabaseHandler("sentiment.db");
		databaseHandler.initialiseDatabase(true);
		
		try {
			//first add all the labels
			for(int i=2; i<data.numAttributes(); i++) {
				databaseHandler.createAttribute(data.attribute(i).name(), i);
			}
			
			for (int i=0; i< data.numInstances();i++) {
				int key = databaseHandler.getKeyForTextEntryValue(data.instance(i).value(0));
				if(key == -1) {					
					key = databaseHandler.createEntry(data.instance(i).stringValue(0), data.instance(i).value(0));
				}
				for(int j=2; j<data.numAttributes(); j++) {
					if(data.instance(i).stringValue(j).equals("1")) {
						databaseHandler.connectEntryAndAttribute(key, j, i);
					}
				}
			}
		
			databaseHandler.close();
		} catch (SQLException e) {
			System.err.println("Error while writing database.");
			e.printStackTrace();
			return;
		}
		try {
			reader.close();
		} catch (IOException e) {
			System.err.println("Error while closing input file ("+sourceFileName+")");
			e.printStackTrace();
		}
		
	}

	private static void createArffFile() {
		try {
			BufferedReader reader = new BufferedReader(new FileReader("cleansed_output.csv"));
			FileWriter writer = new FileWriter("sentiment.arff");
			writer.write("@relation sentiment\n\n");
			writer.write("@attribute contentText string\n");
			writer.write("@attribute contentDate date 'yyyy-MM-dd\'T\'HH:mm:ss'\n");
			writer.write("@attribute positive {0,1}\n");
			writer.write("@attribute neutral {0,1}\n");
			writer.write("@attribute negative {0,1}\n\n");
			writer.write("@data\n");
			
			String line = "";
			while((line = reader.readLine())!=null) {
				
				String[] data = line.split("\t");
				writer.write("{0 '"+data[3].replace("'", "\\'")+"',1 2015-01-22T00:00:00,");
				switch(data[2]) {
					case "positive":
						writer.write("2 1");
						break;
					case "neutral":
						writer.write("3 1");
						break;
					case "negative":
						writer.write("4 1");
						break;
					default:
						System.err.println("Unknown class: "+data[2]);
				}
				writer.write("}\n");
//				System.out.println(data[2]+ " "+data[3]);
			
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
