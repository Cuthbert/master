package ma.modules.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

import ma.annotations.Measure;
import ma.annotations.Trace;
import ma.aspects.Logging;
import ma.db.ArffDatabaseHandler;
import ma.kernel.BOWStringKernel;
import ma.models.AnnotationModel;
import ma.models.SentenceModel;
import ma.tools.MeasureCollector;
import ma.tools.Sorter;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import biz.source_code.base64Coder.Base64Coder;

import com.panayotis.gnuplot.JavaPlot;
import com.panayotis.gnuplot.layout.StripeLayout;
import com.panayotis.gnuplot.plot.AbstractPlot;
import com.panayotis.gnuplot.plot.DataSetPlot;
import com.panayotis.gnuplot.style.PlotStyle;
import com.panayotis.gnuplot.style.Style;
import com.panayotis.gnuplot.terminal.ImageTerminal;

import edu.berkeley.nlp.syntax.Tree;

public class Scratchbook {	
	@SuppressWarnings("unchecked")
	public static void wordExtractor() {
		ArffDatabaseHandler handler = new ArffDatabaseHandler("/home/cuthbert/Dropbox/Uni/Masterarbeit/Programme/automotive.db");
		HashSet<String> bag = new HashSet<>();
		try {
			FileWriter writer = new FileWriter("bag.txt");
			LinkedList<SentenceModel> linkedList = (LinkedList<SentenceModel>) handler.getSentences();
			long words = 0;
			long newWords = 0;
			long sentences = 0;
			for(SentenceModel sentenceModel:linkedList) {
				if(sentenceModel.getSerialisedCleanedTree()==null) continue;
				sentences++;
				List<Tree<String>> terminals = ((Tree<String>) Base64Coder.fromString(sentenceModel.getSerialisedCleanedTree())).getTerminals();
				for(Tree<String> terminal:terminals) {
					words++;
					String word = terminal.getLabel();
					word=word.replaceAll("[\\.\\?!;]", "");
					word=word.toLowerCase();
					if(word.matches("^[a-z]*$") && word.length()>1 && !bag.contains(word)) {
						bag.add(word);
						writer.append(word+"\n");
						newWords++;
					}
				}
				if(sentences%100==0) {
					System.out.println(newWords+" / "+words+" = "+1.0*newWords/words);
				}
			}
			writer.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void lemmaExtractor() {
		ArffDatabaseHandler handler = new ArffDatabaseHandler("/home/cuthbert/Dropbox/Uni/Masterarbeit/Programme/automotive.db");
		HashSet<String> bag = new HashSet<>();
		HashMap<String, Integer> bagCount = new HashMap<>();
		try {
			FileWriter writer = new FileWriter("lemmabag.txt");
			List<SentenceModel> linkedList = (List<SentenceModel>) handler.getSentences(false, 0, null, "valid");
			long words = 0;
			long newWords = 0;
			long sentences = 0;
			for(SentenceModel sentenceModel:linkedList) {
				if(sentenceModel.getAnnotations()==null || sentenceModel.getAnnotations().size()==0) continue;
				sentences++;
				
				for(AnnotationModel annotation:sentenceModel.getAnnotations()) {
					words++;
					String lemma = annotation.getLemma();
					lemma=lemma.replaceAll("[\\.\\?!;]", "");
					lemma=lemma.toLowerCase();
					if(lemma.matches("^[a-z]*$") && lemma.length()>3 && !bag.contains(lemma)) {
						bag.add(lemma);
						bagCount.put(lemma, 0);
						writer.append(lemma+"\n");
						newWords++;
					}
					if(bagCount.containsKey(lemma)) bagCount.put(lemma, new Integer(bagCount.get(lemma)+1));
				}
				if(sentences%100==0) {
					System.out.println(newWords+" / "+words+" = "+1.0*newWords/words);
				}
			}
			writer.close();
			
			writer = new FileWriter("lemmabagcount.txt");
			for(String lemma:bagCount.keySet()) {
				writer.write(lemma+" "+bagCount.get(lemma)+"\n");
			}
			writer.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void lemmaViewer() {
		ArffDatabaseHandler handler = new ArffDatabaseHandler("/home/cuthbert/Dropbox/Uni/Masterarbeit/Programme/automotive.db");
		try {
			List<SentenceModel> linkedList = (List<SentenceModel>) handler.getSentences(false, 10, null, "valid");
			for(SentenceModel sentenceModel:linkedList) {
				if(sentenceModel.getAnnotations()==null || sentenceModel.getAnnotations().size()==0) continue;
				
				for(AnnotationModel annotation:sentenceModel.getAnnotations()) {
					System.out.println(annotation.toString());
					
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void lemmaBagStatistics() {
		try {
			BufferedReader reader = new BufferedReader(new FileReader("allLemmataCount.txt"));
			String line = null;
			HashMap<String, Integer> bagCount = new HashMap<>();
			while((line=reader.readLine())!=null) {
				String[] data = line.split(" ");
				bagCount.put(data[0], Integer.parseInt(data[1]));
			}
			
			int[] border = {2,3,5,7,11,13,17,19,23,29,31,37,41,43,47};
			FileWriter[] writer = new FileWriter[border.length];
			for(int i=0;i<border.length;i++) {
				writer[i] = new FileWriter("lemmaBag"+border[i]+".txt");
				int count = 0;
				for(String key:bagCount.keySet()) {
					if(bagCount.get(key)>=border[i]) {
						count++;
						writer[i].write(key+"\n");
					}
					
				}
				writer[i].close();
				System.out.println("Anzahl an Wörtern mit >= "+border[i]+" Vorkommen: "+count+"/"+bagCount.size());
			}
			reader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void testKernel() {
		ArffDatabaseHandler handler = new ArffDatabaseHandler("/home/cuthbert/Dropbox/Uni/Masterarbeit/Programme/automotive.db");
		try {
			FileWriter writer = new FileWriter("bag.txt");
			LinkedList<SentenceModel> linkedList = (LinkedList<SentenceModel>) handler.getUsableSentences(100, true, 0.5, "BMW_product");
			for(int i=0; i<linkedList.size();i++) {
				for(int j=i+1; j<linkedList.size();j++) {
					BOWStringKernel kernel = new BOWStringKernel();
					kernel.init(linkedList.get(i), linkedList.get(j));
					kernel.setBag(new HashSet<String>());
					kernel.compute();
				}
			}
			writer.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
	/**
	 * @param args
	 */
	public static void main(String[] args) {
//        lemmaExtractor();
//		testTreeMapSorter();
		DOMConfigurator.configureAndWatch("log4j.xml", 60*1000);
		Logging.setLogger(Logger.getRootLogger());
		lemmaViewer();
    }
}
