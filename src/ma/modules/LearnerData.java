package ma.modules;

import java.io.File;
import java.util.Date;
import java.util.List;

import ma.models.TextModel;

public class LearnerData {
	public Date executionDate;
	public File outputDirectory;
	
	public int configIndex;
	
	//Eigentlich nicht nötig, aber besser als einen Standardnamen zu nehmen und dann mehrere zu benötigen...
	public String persistantFileName = null;
	
	public List<TextModel> t;
	
	@SuppressWarnings("unchecked")
	public <T extends TextModel> void setT(List<T> list) {
		t = (List<TextModel>) list;
	}
}
