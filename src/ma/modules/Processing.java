package ma.modules;

import edu.berkeley.nlp.syntax.Tree;
import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CorpusController;
import gate.Document;
import gate.Factory;
import gate.Gate;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.persist.PersistenceException;
import gate.util.GateException;
import gate.util.persistence.PersistenceManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import ma.Options;
import ma.annotations.Measure;
import ma.annotations.Trace;
import ma.aspects.Logging;
import ma.berkeley.BerkeleyParserWrapper;
import ma.db.ArffDatabaseHandler;
import ma.kernel.BOWLemmaStringKernel;
import ma.kernel.BOWStringKernel;
import ma.kernel.CombinedKernel;
import ma.kernel.DirtyBOWStringKernel;
import ma.kernel.EntryBOWLemmaKernel;
import ma.kernel.EntryKernel;
import ma.kernel.LemmaSyntacticTreeKernel;
import ma.kernel.SyntacticTreeKernel;
import ma.models.AnnotationModel;
import ma.models.ClassificationResultModel;
import ma.models.EntryModel;
import ma.models.SentenceModel;
import ma.models.TextModel;
import ma.tools.OptionVariator;
import ma.tools.SentenceCleaner;
import ma.tools.files.FileHandler;
import ma.tools.files.StatisticFileWriter;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.xml.DOMConfigurator;

import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;
import biz.source_code.base64Coder.Base64Coder;

public class Processing {
	private static Properties configuration = null;
	private static Logger logger;
	
	@Trace
	private static void readConfig(String fileName, Options options) {
		configuration = new Properties();
		try {
			configuration.load(new FileReader(fileName));
			options.outputInterval = Integer.parseInt(configuration.getProperty("settings.interval.output", "1000"));
			options.commitInterval = Integer.parseInt(configuration.getProperty("settings.interval.commit", "100"));
			options.statsInterval = Integer.parseInt(configuration.getProperty("settings.interval.stats", "10000"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@Trace
	@Measure
	public static void initialiseDatabase(Options options) {
		String sourceFileName = options.inputFile;
		BufferedReader reader;
		// init source file
		File sourceFile = new File(sourceFileName);
		
		ArffReader arff;
		try {
			reader = new BufferedReader(new FileReader(sourceFile));
			arff = new ArffReader(reader);
		} catch (IOException e) {
			System.err.println("Error while opening input file ("+options.inputFile+")");
			e.printStackTrace();
			return;
		}
		Instances data = arff.getData();
		
		//init database
		ArffDatabaseHandler databaseHandler = new ArffDatabaseHandler(options.databasePath);
		databaseHandler.initialiseDatabase(true);
		
		try {
			//first add all the labels
			for(int i=2; i<data.numAttributes(); i++) {
				if(i%options.outputInterval==0) {
					System.out.println(i);
				}
				databaseHandler.createAttribute(data.attribute(i).name(), i);
			}
			
			for (int i=0; i< data.numInstances();i++) {
				int key = databaseHandler.getKeyForTextEntryValue(data.instance(i).value(0));
				if(key == -1) {					
					key = databaseHandler.createEntry(data.instance(i).stringValue(0), data.instance(i).value(0));
				}
				for(int j=2; j<data.numAttributes(); j++) {
					if(data.instance(i).stringValue(j).equals("1")) {
						databaseHandler.connectEntryAndAttribute(key, j, i);
					}
				}
				if(i%options.outputInterval==0) {
					System.out.println(i);
				}	
			}
		
			databaseHandler.close();
		} catch (SQLException e) {
			System.err.println("Error while writing database.");
			e.printStackTrace();
			return;
		}
		try {
			reader.close();
		} catch (IOException e) {
			System.err.println("Error while closing input file ("+options.inputFile+")");
			e.printStackTrace();
		}

	}
	
	@Trace
	@Measure
	public static void splitEntries(Options options) {
		ArffDatabaseHandler databaseHandler = new ArffDatabaseHandler(options.databasePath);
		HashMap<Integer, String> data;
		try {
			data = databaseHandler.getEntries(!options.refresh);
		} catch (SQLException e1) {
			System.err.println("Error while fetching data from database.");
			e1.printStackTrace();
			return;
		}
		
		HashMap<String, EntryModel> entries = new HashMap<String, EntryModel>();
		
		Gate.setGateHome(new File("/home/cuthbert/Programme/GATE_Developer_8.0"));
		try {
			Gate.init();
		} catch (GateException e) {
			System.err.println("Error while GATE initialisation.");
			e.printStackTrace();
			return;
		}
		CorpusController splittingController;
		try {
			splittingController = (CorpusController)PersistenceManager.loadObjectFromFile(new File(configuration.getProperty("gate.file.splitter")));
		} catch (PersistenceException|ResourceInstantiationException|IOException e) {
			System.err.println("Error while loading GATE controller ("+configuration.getProperty("gate.file.splitter")+")");
			e.printStackTrace();
			return;
		}
		
		try {
			splittingController.setCorpus(Factory.newCorpus("splitcorpus"));
		
			for (Integer entryKey : data.keySet()) {
				String entryDocName = "entry_"+entryKey;
				EntryModel entryModel = new EntryModel();
				entryModel.setId(entryKey);
				entryModel.setText(data.get(entryKey));
				
				Document document = Factory.newDocument(data.get(entryKey));
				document.setName(entryDocName);
				splittingController.getCorpus().add(document);
				entries.put(entryDocName, entryModel);
			}
		} catch (ResourceInstantiationException e) {
			System.err.println("Error while creating corpus data.");
			e.printStackTrace();
			return;
		}
		
		
		try {
			splittingController.execute();
		} catch (ExecutionException e1) {
			System.err.println("Error while running splitting process");
			e1.printStackTrace();
			return;
		}
		Corpus splittingCorpus = splittingController.getCorpus();
		SentenceCleaner sentenceCleaner = new SentenceCleaner();
		
		
		int i=0;
		int sentenceCount = 0;
		for(Document document:splittingCorpus) {
			AnnotationSet annotationSet = document.getAnnotations();

			LinkedList<SentenceModel> sentences = new LinkedList<SentenceModel>();
			String text = document.getContent().toString();
			for(Annotation annotation: annotationSet) {
				if(!annotation.getType().equals("Sentence")) continue;
				sentenceCount++;
				SentenceModel model = new SentenceModel();
				model.setText(text.substring(annotation.getStartNode().getOffset().intValue(), annotation.getEndNode().getOffset().intValue()));
				model.setCleanedText(sentenceCleaner.clean(model.getText()));
				sentences.add(model);
			}
			entries.get(document.getName()).setSentences(sentences);
			i++;
			
			if(i%1000==0) {
				System.out.println(i+" ("+sentenceCount+")");
			}
		}
		
		try {
			int entryCount = 0;
			for (EntryModel entryModel : entries.values()) {
				databaseHandler.updateEntry(entryModel);
				entryCount++;
				if(entryCount%options.outputInterval==0) {
					System.out.println(entryCount);
				}
			}
		
			databaseHandler.close();
		} catch (SQLException e) {
			System.err.println("Error while updating database.");
			e.printStackTrace();
		}
	}
	
	@Trace
	@Measure
	public static void annotateSentences(Options options) {
		ArffDatabaseHandler databaseHandler = new ArffDatabaseHandler(options.databasePath);
		List<SentenceModel> data;
		try {
			data = databaseHandler.getSentences(false, options.noOfTrainingInstances, null, "valid");
		} catch (SQLException e1) {
			System.err.println("Error while fetching data from database.");
			e1.printStackTrace();
			return;
		}
		
		Gate.setGateHome(new File("/home/cuthbert/Programme/GATE_Developer_8.0"));
		try {
			Gate.init();
		} catch (GateException e) {
			System.err.println("Error while GATE initialisation.");
			e.printStackTrace();
			return;
		}
		CorpusController annotatingController;
		try {
			annotatingController = (CorpusController)PersistenceManager.loadObjectFromFile(new File(configuration.getProperty("gate.file.annotator")));
		} catch (PersistenceException|ResourceInstantiationException|IOException e) {
			System.err.println("Error while loading GATE controller ("+configuration.getProperty("gate.file.annotator")+")");
			e.printStackTrace();
			return;
		}
		
		int indexSentences = 0;
		
		while(data.size()>indexSentences) {
			Map<String, SentenceModel> sentences = new HashMap<String, SentenceModel>();
			int step = 100;
			
			try {
				annotatingController.setCorpus(Factory.newCorpus("annotatorcorpus"));
				int counter = 0;
				int countall = 0;
				while(counter<step && data.size()>indexSentences) {
					SentenceModel sentence = data.get(indexSentences);
					indexSentences++;
					countall++;
					//Prune annotated
					if(sentence.getAnnotations().size()>0) {
						continue;
					}
					counter++;
					String sentenceId = "sentence_"+sentence.getId();
					
					Document document = Factory.newDocument(sentence.getCleanedText());
					sentences.put(sentenceId, sentence);
					document.setName(sentenceId);
					annotatingController.getCorpus().add(document);
				}
				System.out.println("Added "+counter+" / "+countall);
			} catch (ResourceInstantiationException e) {
				System.err.println("Error while creating corpus data.");
				e.printStackTrace();
				return;
			}
			
			
			try {
				annotatingController.execute();
			} catch (ExecutionException e1) {
				System.err.println("Error while running splitting process");
				e1.printStackTrace();
				return;
			}
			Corpus annotatedCorpusCorpus = annotatingController.getCorpus();
			
			

			int sentenceCount = 0;
			for(Document document:annotatedCorpusCorpus) {
//				System.out.println(document.getName());
				AnnotationSet annotationSet = document.getAnnotations();
				for(Annotation annotation: annotationSet) {
					if(!annotation.getType().equals("Token")) continue;
					sentenceCount++;
//					System.out.println("AnnotationType: "+annotation.getType());
					Set<String> show = new HashSet<String>();
					show.add("lemma");
					show.add("category");
					show.add("kind");
					show.add("string");
					AnnotationModel annotationModel = new AnnotationModel();
					for(Object annotationKey :annotation.getFeatures().keySet()) {
						if(!show.contains(annotationKey)) continue;
						switch((String)annotationKey) {
							case "lemma":
								annotationModel.setLemma((String)annotation.getFeatures().get(annotationKey));
								break;
							case "string":
								annotationModel.setText((String)annotation.getFeatures().get(annotationKey));
								break;
							case "category":
								annotationModel.setTag((String)annotation.getFeatures().get(annotationKey));
								break;
						}
//						System.out.print("\t"+annotationKey+": "+annotation.getFeatures().get(annotationKey));
					}
//					System.out.println("\t>>\t"+annotationModel);
					sentences.get(document.getName()).addAnnotation(annotationModel);
				}
				
				if(sentenceCount%1000==0) {
					System.out.println(sentenceCount);
				}
			}
			
			sentenceCount = 0;
			try {
				for (SentenceModel sentenceModel : sentences.values()) {
					databaseHandler.updateSentence(sentenceModel);
					sentenceCount++;
					if(sentenceCount%options.outputInterval==0) {
						System.out.println(sentenceCount);
					}
				}
			} catch (SQLException e) {
				System.err.println("Error while updating database.");
				e.printStackTrace();
			}
		}
		try {
			databaseHandler.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	@Trace
	@Measure
	public static void createTrees(Options options) {
		
		BerkeleyParserWrapper berkeleyParserWrapper = new BerkeleyParserWrapper();
		berkeleyParserWrapper.init(configuration.getProperty("berkeley.grammar-file"));
		
		try {
			
			ArffDatabaseHandler databaseHandler = new ArffDatabaseHandler(options.databasePath);
			List<SentenceModel> data = databaseHandler.getSentences(!options.refresh, options.noOfTrainingInstances);
			
			
			
			int entryCount = 0;
			for (SentenceModel sentenceModel : data) {
				
				List<Tree<String>> result = berkeleyParserWrapper.parse(sentenceModel.getCleanedText());
				
				sentenceModel.setParsedTree(result.get(0).toString());
				
				sentenceModel.setSerialisedTree(Base64Coder.toString(result.get(0)));
				
				
				databaseHandler.updateSentence(sentenceModel);
				
				
				entryCount++;
				if(entryCount%options.outputInterval==0) {
					System.out.println(entryCount+" ("+ (entryCount-entryCount%options.commitInterval) +" committed)");
				}
				if(entryCount%options.commitInterval==0) {
					databaseHandler.commit();
				}
			}		
			
			
			databaseHandler.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Trace
	@Measure
	@SuppressWarnings("unchecked")
	public static Tree<String> decodeAndReduceTree(SentenceModel sentenceModel) {
		if(!sentenceModel.hasSerialisedTree()) return null;
		try {
			
			Tree<String> tempTree =(Tree<String>) Base64Coder.fromString(sentenceModel.getSerialisedTree());
//			checkTree(tempTree, entryId);
			if(tempTree.getLabel().equals("ROOT") && tempTree.getChildren().size()>0) {
				tempTree=tempTree.getChild(0);
			}
			if(tempTree.getLabel().equals("PSEUDO") && tempTree.getChildren().size()>0) {
				tempTree=tempTree.getChild(0);
			}
			if(!tempTree.getLabel().equals("S")) return null;
			if(tempTree.getChildren().size()==0) return null;
			//returns tree temporary to model
			
			return tempTree;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
		
	@Trace
	@Measure
	public static void validateSentences(String databasePath) {
		ArffDatabaseHandler databaseHandler = new ArffDatabaseHandler(databasePath);
		try {
			List<SentenceModel> sentences = databaseHandler.getSentences(false, 0, null, "new");
			int count=0;
			int countDone=0;
			for(SentenceModel sentence : sentences) {
				Tree<String> reduced = decodeAndReduceTree(sentence);
				if(reduced!=null) {
					sentence.setSerialisedCleanedTree(Base64Coder.toString(reduced));
					sentence.setStatus("valid");
					countDone++;
				} else {
					sentence.setStatus("invalid");
				}
				databaseHandler.updateSentence(sentence);
				count++;
				if(count%1000==0) System.out.println(countDone+"/"+count);
			}
			databaseHandler.commit();
			databaseHandler.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	public static void main(String[] args) {
		DOMConfigurator.configureAndWatch("log4j.xml", 60*1000);
		logger = Logger.getRootLogger();
		
		Map<String,String> commands = new HashMap<String, String>();
		commands.put("-d", "Database path");
		commands.put("-r", "Refresh flag (0|1)");
		commands.put("-i", "Input file");
//		commands.put("-o", "Output file / Prefix for output files");
		commands.put("-l", "Limit # of entries");
		commands.put("-k", "Kernel identifier (1=SyntacticTreeKernel, 2=LemmaSyntacticTree, 3=BOWStringKernel)");
		commands.put("-label", "A label name");
		commands.put("-ratio", "Set ratio of positive classified examples [0,1], Standard value is ratio from data");
		commands.put("-iterations", "# of maximum iterations");
		commands.put("-add", "# of entries to add per iteration (e.g. kernel matrix expansion)");
		commands.put("-t", "Threshold (e.g. 'learn': minimum accuracy");
		commands.put("-printlines", "Log all classification details for each instance (0|1)");
		commands.put("-srl", "minimum # of consecutive smaller or equal ratios (e.g. accuracy) as stop criteria");
		commands.put("-lambda", "Lambda value for combined kernels");
		commands.put("-random", "Select random elements (0|1)");
		commands.put("-noOfTestinstances", "# of selected test instances");
		
		
		Options options = createOptions(args);
		readConfig("config.cfg", options);
		
		String command = "help";
		if(args.length>=1) {
			command = args[0];
		}
				
		/*if(!command.equals("help")) {
			logger.info("Parameter:\n"+options.values());
		}*/
		switch(command) {
			case "dbinit":
				if(options.inputFile!=null && options.databasePath!=null) {
					initialiseDatabase(options);
				} else {
					System.err.println("Please provide input file and database location (sqlite)");
					return;
				}
				break;
			case "annotator":
				if(options.databasePath!=null) {
					annotateSentences(options);
				} else {
					System.err.println("Please provide database location (sqlite) and [refresh setting]");
					return;
				}
				break;
			case "splitter":
				if(options.databasePath!=null) {
					splitEntries(options);
				} else {
					System.err.println("Please provide database location (sqlite) and [refresh setting]");
					return;
				}
				break;
			case "trees":
				if(options.databasePath!=null) {
					createTrees(options);
				} else {
					System.err.println("Please provide database location (sqlite) and [refresh setting]");
					return;
				}
				break;
			case "validateSentences":
				if(options.databasePath!=null) {
					validateSentences(options.databasePath);
				} else {
					System.err.println("Please provide database location (sqlite) and [refresh setting]");
					return;
				}
				break;
			case "learn":
				/*if(options.databasePath!=null && 
				options.kernelInstance!=null && 
				(options.inputFile!=null || (options.readLimit>0 && options.outputFile!=null)) &&
				options.label!=null &&
				options.expansionSize>0 &&
				(options.threshold>0 || options.iterations>0)) {*/
					
					
					LearnerData learnerData = new LearnerData();
					learnerData.executionDate = new Date(System.currentTimeMillis());
					SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					learnerData.outputDirectory = new File(configuration.getProperty("basedir", "log/")+options.setupName+"_"+dt.format(learnerData.executionDate));
					learnerData.outputDirectory.mkdir();
					learnerData.configIndex = 1;
					
					/**
					 * prepare wrap logger
					 */
					Logger wrapLogger = Logger.getLogger(Processing.class);
					PatternLayout layout = new PatternLayout("%d{ISO8601} %-5p [%t] %c: %m%n");
					try {
						wrapLogger.addAppender(new FileAppender(layout, learnerData.outputDirectory.getPath()+"/wrap.log", true));
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					/**
					 * Prepare variations
					 */
					OptionVariator optionVariator = new OptionVariator(options);
					optionVariator.setLogger(wrapLogger);
					Logging.setLogger(wrapLogger);
					Options vOptions;
					wrapLogger.info("Variationen: "+optionVariator.getNumberOfVariations());
					StatisticFileWriter statisticFile = new StatisticFileWriter(learnerData.outputDirectory);
					
					int lastNumOfInstances = -1;
					
					while((vOptions = optionVariator.next())!=null) {
						if(optionVariator.isFirstIteration()) {
							learnerData.t = null;
							wrapLogger.info("Request new test data");
						}
						initKernel(vOptions);
						wrapLogger.info(vOptions.values());
						@SuppressWarnings("rawtypes")
						LearnerModule learnerModule;
						if(vOptions.kernelInstance.getKernelDataType()==1) {
							learnerModule = new LearnerModule<SentenceModel>();
						} else if(vOptions.kernelInstance.getKernelDataType()==2) {
							learnerModule = new LearnerModule<EntryModel>();
						} else {
							wrapLogger.error("Invalid kernel or missing kernel data type");
							break;
						}
						if(optionVariator.getIterationNumber()==1) {
							statisticFile.writeOptions(vOptions);
						}
//						System.out.println(vOptions.values());

						
						if(vOptions.classificator.equals("SVM_SIMPLE")) {
							vOptions.noOfTrainingInstances = lastNumOfInstances;
							vOptions.isSimpleSVM = true;
							vOptions.classificator = Options.SVM;
						}
						@SuppressWarnings("unchecked")
						ClassificationResultModel<TextModel> resultModel = learnerModule.learn(vOptions, learnerData);
						//if everything went right
						if(resultModel!=null) {
							statisticFile.write(resultModel, optionVariator.getIterationNumber(),-1,-1,vOptions.optionId);
							lastNumOfInstances = resultModel.lastNumberOfTrainingData;
						}
						
						learnerData.configIndex++;
					}
					statisticFile.close();
				break;
			case "help":
			default:
				System.out.println("Enter command. \n\te.g.:"+Processing.class.getSimpleName()+" command [options]");
				System.out.println("The following commands are possible:");
				System.out.println("dbinit -i input_file -d database_location\n\tCreates a new database file and inserts the data from the given input file (arff)");
				System.out.println("splitter -d database_location -r [0|1]\n\tSplits all entries in sentences and optionally overwrites existing sentences");
				System.out.println("trees -d database_location -r [0|1]\n\tCreates syntactic tress for all sentences and optionally overwrites existing trees");
				System.out.println("help\n\tThis overview.\n\n");
				System.out.println("All options:");
				for(String key:commands.keySet()) {
					System.out.println(key+":\n\t"+commands.get(key));
				}
				break;
		}
		
		BasicModule.printMeasures();
		
		//Wait for logs to be written (http://stackoverflow.com/questions/24215759/log4j2-asynclogger-not-logging-full-data)
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Trace
	private static Options createOptions(String[] args) {
		int i=1;
	
		Options options = new Options();
		//search for config file, remaining parameters alter given parameters from config
		for(int c=0; c<args.length; c++) {
			if(args[c].equals("-setup")) {
				options.loadSetup(args[c+1]);
				break;
			}
		}
		
		while(i<args.length && (i+1)<args.length) {
			switch(args[i]) {
				case "-d":
					options.databasePath = args[i+1];
					break;
				case "-r":
					options.refresh = args[i+1].equals("1");
					break;
				case "-i":
					options.inputFile = args[i+1];
					break;
/*				case "-o":
					options.outputFile = args[i+1];
					break;*/
				case "-l":
					options.noOfTrainingInstances = Integer.parseInt(args[i+1]);
					break;
				case "-k":
					options.kernel = args[i+1];
					options.kernelVariation = null;
					break;
				case "-label":
					options.label = args[i+1];
					options.labelVariation = null;
					break;
				case "-ratio":
					options.ratio = Double.parseDouble(args[i+1]);
					options.ratioVariation = null;
					break;
				case "-iterations":
					options.iterations = Integer.parseInt(args[i+1]);
					break;
				case "-add":
					options.expansionSize = Integer.parseInt(args[i+1]);
					options.expansionSizeVariation = null;
					break;
				case "-t":
					options.threshold = Double.parseDouble(args[i+1]);
					break;
				case "-printlines":
					options.logLines = args[i+1].equals("1");
					break;
				case "-srl":
					options.smallerRatioLimit = Integer.parseInt(args[i+1]);
					break;
				case "-lambda":
					options.lambda = Double.parseDouble(args[i+1]);
					options.lambdaVaration = null;
					break;
				case "-random":
					options.randomSelection = args[i+1].equals("1");
					break;
				case "-normalize":
					options.normalize = args[i+1].equals("1");
					break;
				case "-noOfTestinstances":
					options.noOfTestinstances = Integer.parseInt(args[i+1]);
					break;
			}
			i+=2;
		}
		
//		initKernel(options);
		return options;
	}

	@Trace
	@Measure
	private static void initKernel(Options options) {
		//initializing kernel
		switch(options.kernel) {
			case "1":
				options.kernelInstance = new SyntacticTreeKernel();
				break;
			case "2":
				options.kernelInstance = new LemmaSyntacticTreeKernel();
				break;
			case "3":
				options.kernelInstance = new BOWStringKernel();
				break;
			case "4":
				options.kernelInstance = new CombinedKernel(options.lambda, new BOWStringKernel(), new SyntacticTreeKernel());						
				break;
			case "5":
				options.kernelInstance = new BOWLemmaStringKernel();
				break;
			case "6":
				options.kernelInstance = new CombinedKernel(options.lambda, new BOWLemmaStringKernel(), new SyntacticTreeKernel());
				break;
			case "13":
				options.kernelInstance = new DirtyBOWStringKernel();
				break;
			case "102":
				options.kernelInstance = new EntryKernel(new LemmaSyntacticTreeKernel());
				break;
			case "105":
				options.kernelInstance = new EntryKernel(new BOWLemmaStringKernel());
				break;
			case "200":
				options.kernelInstance = new EntryBOWLemmaKernel();
				break;
			default:
				logger.error("Invalid kernel identifier ["+options.kernel+"]");
				break;
		}
	}
}