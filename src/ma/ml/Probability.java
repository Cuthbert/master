package ma.ml;

import ma.annotations.Measure;
import ma.annotations.Trace;

public class Probability {
	@Measure
	@Trace
	public static double[] plattScaling(double[] pF, int[] pClass) {
		if(pF.length!=pClass.length) {
			return null;
		}
		
		int length = pF.length;
		
		double[] probability = new double[length];
		
		
		//calculate sigmoid
		double noOfPositive = 0; //prior1
		double noOfNegative = 0; //prior0
		//count class distribution
		for(int i=0; i<length;i++) {
			if(pClass[i]==1) noOfPositive++; else noOfNegative++;
		}
		double startProbability = (noOfPositive+1)/(noOfNegative+noOfPositive+2);
		for(int i=0; i<length;i++) {
			probability[i] = startProbability;
		}
		
		//INITIALISE PARAMETERS
		//sigmoid params
		double A = 0;
		double B = Math.log((noOfNegative+1)/(noOfPositive+1));
		
		double t_plus = (noOfPositive+1)/(noOfPositive+2);
		double t_minus = 1/(noOfNegative+2);
		
		double lambda = 1e-3;
		double oldError = 1e300;
		
		int count = 0;
		
		for(int iterator=0; iterator<100; iterator++) {
			double a=0, b=0, c=0, d=0, e=0;
			
			// First, compute Hessian & gradient of error function
			// with respect to A & B
			for(int i=0; i<length;i++) {
				double t=0, d1=0, d2=0;
				
				t = pClass[i]==1?t_plus:t_minus;
				
				d1 = probability[i]-t;
				d2 = probability[i]*(1-probability[i]);
				a += pF[i]*pF[i]*d2;
				b += d2;
				c += pF[i]*d2;
				d += pF[i]*d1;
				e += d1;
			}
			// If gradient is really tiny, then stop
			if (Math.abs(d) < 1e-9 && Math.abs(e) < 1e-9) break;
			
			double oldA = A;
			double oldB = B;
			double err = 0;
			
			// Loop until goodness of fit increases
			while(true) {
				double det = (a+lambda)*(b+lambda)-c*c;
				// if determinant of Hessian is zero,
				// increase stabilizer
				if (det == 0) { 
					lambda *= 10;
					continue;
				}
				
				A = oldA + ((b+lambda)*d-c*e)/det;
				B = oldB + ((a+lambda)*e-c*d)/det;
				
				// Now, compute the goodness of fit
				err = 0;
				for(int i = 0; i<length;i++) {
					double t = pClass[i]==1?t_plus:t_minus;
					probability[i] = 1/(1+Math.exp(pF[i]*A+B));

					// At this step, make sure log(0) returns -200
					err -= t*Math.log(probability[i])+(1-t)*Math.log(1-probability[i]);
				}
				if (err < oldError*(1+1e-7)) {
					lambda *= 0.1;
					break;
				}
				
				// error did not decrease: increase stabilizer by factor of 10
				// & try again
				lambda *= 10;
				if (lambda >= 1e6) break;// something is broken. Give up
			}
			
			double diff = err-oldError;
			double scale = 0.5*(err+oldError+1);
			if (diff > -1e-3*scale && diff < 1e-7*scale) count++; else count = 0;
			oldError = err;
			if (count == 3) break;
		}
		
		return probability;
	}
}
