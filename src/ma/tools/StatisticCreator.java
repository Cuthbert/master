package ma.tools;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import ma.models.ClassificationResultModel;
import ma.models.TextModel;
import ma.tools.files.StatisticFileReader;
import weka.experiment.PairedStats;
import weka.experiment.PairedStatsCorrected;
import org.apache.commons.math3.stat.inference.WilcoxonSignedRankTest;

public class StatisticCreator {
	public static final int KEY_ACCURACY = 1;
	public static final int KEY_AUC = 2;
	public static final int KEY_PRECISION = 3;
	public static final int KEY_NPRECISION = 4;
	public static final int KEY_RECALL = 5;
	public static final int KEY_F1 = 6;
	
	StatisticFileReader statisticFileReader;
	
	public StatisticCreator(StatisticFileReader statisticFileReader) {
		this.statisticFileReader = statisticFileReader;
	}
	
	public List<String> createMethodCompare(String groupField, int valueKey) {
		List<String> writeData = new LinkedList<>();
				
		double[][] values = new double[statisticFileReader.entries.size()][statisticFileReader.getIterationCount()];
		LinkedHashMap<String, LinkedList<String>> data = new LinkedHashMap<>();
		double[][] statistics = new double[statisticFileReader.entries.size()][];
		
		int s = 0;
		for(String setupId:statisticFileReader.setupData.keySet()) {
			for(int it=0; it<statisticFileReader.getIterationCount();it++) {
				ClassificationResultModel<TextModel> tempResultModel = statisticFileReader.entries.get(setupId).get(it);
				double value = 0;
				switch(valueKey) {
					case KEY_ACCURACY:
						value = tempResultModel.getAccuracy();
						break;
					case KEY_AUC:
						value = tempResultModel.getAuc();
						break;
					case KEY_RECALL:
						value = tempResultModel.getRecall();
						break;
					case KEY_PRECISION:
						value = tempResultModel.getPrecision();
						break;
					case KEY_NPRECISION:
						value = tempResultModel.getNprecision();
						break;
					case KEY_F1:
						value = tempResultModel.getF1();
						break;
				}
				values[s][it] = value;
			}
			statistics[s] = computeStatistics(values[s]);
			String groupFieldValue = statisticFileReader.setupData.get(setupId).get(groupField); 
			if(!data.containsKey(groupFieldValue)) {
				System.out.println("Neu: "+statisticFileReader.setupData.get(setupId));
				data.put(groupFieldValue, new LinkedList<String>());
			}
			data.get(groupFieldValue).add("#"+setupId+"\n"+groupFieldValue+" "+statistics[s][0]+" "+statistics[s][1]);
			s++;
		}
		
		//getting headline
		List<String> headLine = new LinkedList<>();
		for(String setupId:statisticFileReader.setupData.keySet()) {
			String temp = statisticFileReader.setupData.get(setupId).get("classificator")+"("+statisticFileReader.setupData.get(setupId).get("kernel")+")";
			if(!headLine.contains(temp)) headLine.add(temp);
		}
		for(int i=0; i<headLine.size();i++) {
			writeData.add(headLine.get(i)+" ");
		}
		writeData.add("\n");

		//write the data
		for(String groupValue:data.keySet()) {
			for(int i=0; i<data.get(groupValue).size(); i++) {
				writeData.add(data.get(groupValue).get(i)+"\n");
			}
		}
		
		writeData.add("#statistics_paired_t_test\n");
		
		int i=0;
		for(String setupId:statisticFileReader.setupData.keySet()) {
			int j=-1;
			for(String setupId2:statisticFileReader.setupData.keySet()) {
				j++;
				if(j<=i) continue;
				System.out.println(statisticFileReader.setupData.get(setupId).get("option")+" "+statisticFileReader.setupData.get(setupId2).get("option"));
				writeData.add("#"+setupId+"-"+setupId2+" "+computeSignificance(values[i], values[j])+"\n");
				
			}
			i++;
		}
		writeData.add("#statistics_wilcoxon\n");
		
		i=0;
		for(String setupId:statisticFileReader.setupData.keySet()) {
			int j=-1;
			for(String setupId2:statisticFileReader.setupData.keySet()) {
				j++;
				if(j<=i) continue;
				System.out.println(statisticFileReader.setupData.get(setupId).get("option")+" "+statisticFileReader.setupData.get(setupId2).get("option"));
				double pValue = computeSignificanceWilcoxon(values[i], values[j]);
				writeData.add("#"+setupId+"-"+setupId2+" "+pValue+" "+(pValue<=0.05?"1":"0")+"\n");
				
			}
			i++;
		}
					
		return writeData;
	}
	
	private double[] computeStatistics(double[] values) {
		double[] statistics = new double[2];
		int counter = 0; 
			double temp = 0;
			for(int j=0; j<values.length; j++) {
				if(values[j]>0) {
					temp+=values[j];
					counter++;
				}
			}
			//mean
			statistics[0] = temp/(counter);
			
			temp = 0;
			for(int j=0; j<values.length; j++) {
				if(values[j]>0) {
					temp+=Math.pow(values[j]-statistics[0], 2);
				}
			}
			//Standardabweichung
			statistics[1] = Math.pow(temp/counter, 0.5);
		return statistics;
	}
	
	private double[][] computeStatistics(double[][] values) {
		double[][] statistics = new double[values.length][2];
		int[] counter = new int[values.length]; 
		for(int i=0; i<values.length; i++) {
			statistics[i] = computeStatistics(values[i]);
			/*double temp = 0;
			for(int j=0; j<values[i].length; j++) {
				if(values[i][j]>0) {
					temp+=values[i][j];
					counter[i]++;
				}
			}
			//mean
			statistics[i][0] = temp/(counter[i]);
			
			temp = 0;
			for(int j=0; j<values[i].length; j++) {
				if(values[i][j]>0) {
					temp+=Math.pow(values[i][j]-statistics[i][0], 2);
				}
			}
			//Standardabweichung
			statistics[i][1] = Math.pow(temp/counter[i], 0.5);*/
		}
		return statistics;
	}

	private String computeSignificance(double[] values1, double[] values2) {
		PairedStatsCorrected pairedStatsCorrected = new PairedStatsCorrected(0.05, 0.66);
		pairedStatsCorrected.add(values1, values2);
		pairedStatsCorrected.calculateDerived();
		return pairedStatsCorrected.differencesProbability+" "+pairedStatsCorrected.differencesSignificance;
	}
	private double computeSignificanceWilcoxon(double[] values1, double[] values2) {
		WilcoxonSignedRankTest wt = new WilcoxonSignedRankTest(org.apache.commons.math3.stat.ranking.NaNStrategy.FAILED, org.apache.commons.math3.stat.ranking.TiesStrategy.AVERAGE);

		return wt.wilcoxonSignedRankTest(values1, values2, false);
	}
	
}
