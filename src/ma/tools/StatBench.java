package ma.tools;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;


import ma.tools.files.StatisticFileReader;

public class StatBench {
	public static void main(String[] args) {	
		int[] keys = {StatisticCreator.KEY_AUC, StatisticCreator.KEY_ACCURACY, StatisticCreator.KEY_PRECISION, StatisticCreator.KEY_NPRECISION, StatisticCreator.KEY_RECALL, StatisticCreator.KEY_F1};
		String[] keyNames = {"AUC","ACCURACY","PRECISION","NPRECISION","RECALL","F1"};
		String file = "";
		String variation = "kernel";
		int key = -1;
		
		for(int i=0; i<args.length-1; i++) {
			switch(args[i]) {
				case "-file":
					file=args[i+1];
					break;
				case "-vary":
					variation=args[i+1];
					break;
				case "-key":
					for(int j=0; j<keyNames.length; j++) {
						if(args[i+1].equals(keyNames[j])) key = j;
					}
					break;
			}
		}
		
		StatisticCreator creator = new StatisticCreator(new StatisticFileReader(new File(file)));
		
		try {
			if(key>=0) {
				List<String> writeData = creator.createMethodCompare(variation, keys[key]);
				FileWriter datWriter = new FileWriter(file+"_"+keyNames[key]+".dat");
				for(String s:writeData) {
					datWriter.write(s);
				}
				datWriter.close();
			} else {
				for(int i=0; i<keys.length; i++) {
					List<String> writeData = creator.createMethodCompare(variation, keys[i]);
	//				List<String> writeData = creator.createSetupCompare(variation, keys[i]);
					
					if(false) return;
					FileWriter datWriter = new FileWriter(file+"_"+keyNames[i]+".dat");
					for(String s:writeData) {
						datWriter.write(s);
					}
					datWriter.close();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}