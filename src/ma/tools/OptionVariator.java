package ma.tools;

import java.util.LinkedList;

import ma.Options;
import ma.annotations.Trace;

import org.apache.log4j.Logger;

public class OptionVariator {
	private LinkedList<String> varyIds;
	private int[] indices;
	private LinkedList<Object[]> values;
	private Options basicOptions;
	private int repeatCount = 0;
	boolean done = false;
	Logger logger = Logger.getLogger(OptionVariator.class);
	private boolean firstVariation = false;
	private int optionId = 0;
	
	public OptionVariator(Options options) {
		basicOptions = options;
		initialize(basicOptions);
	}
	
	public void setLogger(Logger logger) {
		this.logger = logger;
	}
	
	private void initialize(Options options) {
		done = false;
		varyIds = new LinkedList<String>();
		values = new LinkedList<Object[]>();
		//count varies, copy values
		if(options.expansionSizeVariation!=null) {
			varyIds.add("expansionSize");
			Integer[] temp = new Integer[options.expansionSizeVariation.length];
			for(int i=0; i<temp.length;i++) temp[i] = options.expansionSizeVariation[i];
			values.add(temp);
		}
		if(options.noOfNewTrainingInstancesVariation!=null) {
			varyIds.add("newTrainingInstances");
			Integer[] temp = new Integer[options.noOfNewTrainingInstancesVariation.length];
			for(int i=0; i<temp.length;i++) temp[i] = options.noOfNewTrainingInstancesVariation[i];
			values.add(temp);
		}
		if(options.kernelVariation!=null) {
			varyIds.add("kernel");
			values.add(options.kernelVariation);
		}
		
		if(options.lambdaVaration!=null) {
			varyIds.add("lambda");
			Double[] temp = new Double[options.lambdaVaration.length];
			for(int i=0; i<temp.length;i++) temp[i] = options.lambdaVaration[i];
			values.add(temp);
		}
		
		if(options.labelVariation!=null) {
			varyIds.add("label");
			values.add(options.labelVariation);
		}
		
		if(options.ratioVariation!=null) {
			varyIds.add("ratio");
			Double[] temp = new Double[options.ratioVariation.length];
			for(int i=0; i<temp.length;i++) temp[i] = options.ratioVariation[i];
			values.add(temp);
		}
		
		if(options.normalizeVariation!=null) {
			varyIds.add("normalized");
			Boolean[] temp = {false,true};
			values.add(temp);
		}
		
		if(options.classificatorVariation!=null) {
			varyIds.add("classificator");
			values.add(options.classificatorVariation);
		}
		
		if(options.randomRatioVariation!=null) {
			varyIds.add("randomRatio");
			Double[] temp = new Double[options.randomRatioVariation.length];
			for(int i=0; i<temp.length;i++) temp[i] = options.randomRatioVariation[i];
			values.add(temp);
		}
		
		if(options.SVMCVariation!=null) {
			varyIds.add("svm_c");
			Double[] temp = new Double[options.SVMCVariation.length];
			for(int i=0; i<temp.length;i++) temp[i] = options.SVMCVariation[i];
			values.add(temp);
		}
				
		if(varyIds.size()==0) return;
		indices = new int[varyIds.size()];
		for(int i=0; i<indices.length;i++) indices[i] = 0;
		firstVariation = true;
		optionId = 1;
	}
	
	@Trace
	public Options next() {
		//check if done
		if(done) {
			repeatCount++;
			//Reset
			if(repeatCount<basicOptions.repeat) {
				this.initialize(basicOptions);
			} else {
				return null;
			}
		}

		//check if new round
		for(int i=0; i<indices.length; i++) {
			if(indices[i]>0) firstVariation = false;
		}
		Options varyOptions = new Options(basicOptions);

		//fill with options
		for(int i=0; i<varyIds.size();i++) {
			Object data = values.get(i)[indices[i]];
			switch(varyIds.get(i)) {
			case "expansionSize":
				varyOptions.expansionSize = (Integer)data;
				break;
			case "newTrainingInstances":
				varyOptions.noOfNewTrainingInstances = (Integer)data;
				break;	
			case "lambda":
				varyOptions.lambda = (Double)data;
				break;
			case "label":
				varyOptions.label = (String)data;
				break;
			case "ratio":
				varyOptions.ratio = (Double)data;
				break;
			case "normalized":
				varyOptions.normalize = (Boolean)data;
				break;
			case "kernel":
				varyOptions.kernel = (String)data;
				break;
			case "classificator":
				varyOptions.classificator = (String)data;
				break;
			case "randomRatio":
				varyOptions.randomRatio = (Double)data;
				break;
			case "svm_c":
				varyOptions.svm_c = (Double)data;
				break;
			}
			
		}
		
		logger.info("Current variation setup:");
		for(int i=0; i<varyIds.size(); i++) {
			logger.info(varyIds.get(i)+": "+values.get(i)[indices[i]]);
		}
		logger.info("Repeat iteration: "+(repeatCount+1)+"/"+basicOptions.repeat);
		
		boolean change = false;
		if(varyIds.size()>0) {			
			//no rising if on last element
			for(int i=0; i<indices.length; i++) {
				if(indices[i]<values.get(i).length-1) {
					indices[i]++;
					change = true;
					break;
				} else if(i<indices.length-1) {
					indices[i] = 0;
				}
			}
		}
		
		varyOptions.optionId = optionId;
		optionId++;
		done = !change;
		return varyOptions;
	}

	public int getNumberOfVariations() {
		int c=1;
		for(int i=0;i<values.size();i++) c*=values.get(i).length;
		return c;
	}

	public boolean isFirstIteration() {
		return firstVariation;
	}

	public int getIterationNumber() {
		return repeatCount+1;
	}
}
