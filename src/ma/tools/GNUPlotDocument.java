package ma.tools;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import ma.annotations.Measure;

import com.panayotis.gnuplot.JavaPlot;
import com.panayotis.gnuplot.JavaPlot.Key;
import com.panayotis.gnuplot.plot.DataSetPlot;
import com.panayotis.gnuplot.style.Style;
import com.panayotis.gnuplot.terminal.ImageTerminal;

public class GNUPlotDocument {
	private File imageFile;
	private JavaPlot plot;
	
	public GNUPlotDocument(File file) {
		this.imageFile = file;
		plot = new JavaPlot();
		plot.setKey(Key.BOTTOM_RIGHT);
		plot.setTerminal(new ImageTerminal());
		plot.set("key", "left reverse Left");
	}
	
	public void defineAxis(double x1, double x2, double y1, double y2) {
		plot.getAxis("x").setBoundaries(x1, x2);
		plot.getAxis("y").setBoundaries(y1, y2);
	}
	
	public void addPlot(DataSetPlot dataSetPlot, String title, Style style, int lineWidth, int pointType, int pointSize) {
		dataSetPlot.setTitle(title);
		dataSetPlot.getPlotStyle().setStyle(style);
		dataSetPlot.getPlotStyle().setLineWidth(lineWidth);
		dataSetPlot.getPlotStyle().setPointType(pointType);
		dataSetPlot.getPlotStyle().setPointSize(pointSize);
		plot.addPlot(dataSetPlot);
	}
	
	@Measure
	public void createImage() {
		plot.plot();
		try {
			ImageIO.write(((ImageTerminal)plot.getTerminal()).getImage(), "png", imageFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
