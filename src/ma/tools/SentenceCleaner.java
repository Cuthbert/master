package ma.tools;

import java.util.ArrayList;

import ma.annotations.Measure;
import ma.annotations.Trace;

/**
 * 
 * @author cuthbert
 *
 */
public class SentenceCleaner {
	/* Features: 
	 * 	-Einsame Klammern entfernen
	 * 	-Begrüßungs-/Verabschiedungsfloskeln entfernen/erkennen
	 *  -Foren-'Code' entfernen ('Zitiert von', etc.)
	 *  -mehrere zusammenhängende Punkte, etc. entfernen
	 *  
	 *  Eventuell über eine Config-Datei?
	 */
	
	private ArrayList<String> removeable = new ArrayList<String>();
	private ArrayList<String[]> replaces = new ArrayList<String[]>();
	
	public SentenceCleaner() {
		removeable.add("Zitat:");
		removeable.add("Original geschrieben von [A-Za-z0-9_]+");
		
		String[] patternQuestion = {"\\s+\\?", "?"};
		replaces.add(patternQuestion);
		String[] patternExclamation = {"\\s+!", "!"};
		replaces.add(patternExclamation);
		String[] patternDot = {"\\s+\\.", "."};
		replaces.add(patternDot);
		String[] patternSlash = {"\\s*\\\\\\s*", "/"};
		replaces.add(patternSlash);
		String[] patternSlash2 = {"/\\s+", "/"};
		replaces.add(patternSlash2);
	}
	
	@Trace
	@Measure
	public String clean(String sentence) {
		String cleanedSentence = sentence;
		try {
//		System.out.println(sentence);
			for(String expr:removeable) {
				cleanedSentence = cleanedSentence.replaceAll(expr, "");
			}
			for(String[] expr:replaces) {
				cleanedSentence = cleanedSentence.replaceAll(expr[0], expr[1]);
			}
		} catch(IndexOutOfBoundsException exception) {
			System.err.println("Something wrong while cleaning:\n\t"+sentence+"\n\t"+cleanedSentence);
		}
//		System.out.println(cleanedSentence);
		return cleanedSentence;
	}
}
