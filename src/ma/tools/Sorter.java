package ma.tools;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import ma.annotations.Measure;

public class Sorter {
	@Measure
	public static int[] treeMapSort(double[] values) {
		int[] sortedIndices = new int[values.length];
	    Map<Double, List<Integer>> map = new TreeMap<Double, List<Integer>>();
	    for (int i = 0; i < values.length; ++i) {
	    	List<Integer> l = new LinkedList<>();
	    	if(map.containsKey(values[i])) {
	    		l = map.get(values[i]);
	    	}
	    	l.add(i);
	        map.put(values[i], l);
	    }

	    int index = 0;
	    for(List<Integer> l : map.values()) {
	    	for(Integer i:l) {
	    		sortedIndices[index] = i;
	    		index++;
	    	}
	    }
	    return sortedIndices;
	}
}
