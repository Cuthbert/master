package ma.tools;

import java.util.HashMap;

public class MeasureTimer {
	private static HashMap<String, Long> startTimes = new HashMap<>();

	public static void init() {
		MeasureCollector.init();
		startTimes = new HashMap<>();
	}
	
	public static void start(String measureId) {
		startTimes.put(measureId, System.currentTimeMillis());
	}
	
	public static void stop(String measureId) {
		if(!startTimes.containsKey(measureId)) return;
		MeasureCollector.measure(measureId, System.currentTimeMillis()-startTimes.get(measureId).longValue());
		startTimes.remove(measureId);
	}
}
