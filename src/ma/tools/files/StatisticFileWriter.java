package ma.tools.files;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import ma.Options;
import ma.models.ClassificationResultModel;
import ma.models.TextModel;

public class StatisticFileWriter {
	private PrintStream printStream;
	public StatisticFileWriter(File dir) {
		try {
			printStream = new PrintStream(dir.getPath()+"/statistics.csv");
			printStream.println("Iteration;Option;#SVs;tp;fp;tn;fn;accuracy;bias;minf;maxf;recall;precision;nprecision;f1;auc");	
		} catch (IOException e3) {
			e3.printStackTrace();
		}
	}
	
	public <T extends TextModel> void write(ClassificationResultModel<T> resultModel, int iterationCount) {
		write(resultModel, iterationCount, -1, -1);
	}
	public <T extends TextModel> void write(ClassificationResultModel<T> resultModel, int iterationCount, int supportVectorCount, double bias) {
		write(resultModel, iterationCount, supportVectorCount, bias, 0);
	}
	public <T extends TextModel> void write(ClassificationResultModel<T> resultModel, int iterationCount, int supportVectorCount, double bias, int optionId) {
		printStream.println(iterationCount+";"+optionId+";"+supportVectorCount+";"+resultModel.tp+";"+resultModel.fp+";"+resultModel.tn+";"+resultModel.fn+";"+Double.toString(resultModel.getAccuracy())+";"+bias+";"+resultModel.minf+";"+resultModel.maxf+";"+resultModel.getRecall()+";"+resultModel.getPrecision()+";"+resultModel.getNprecision()+";"+resultModel.getF1()+";"+resultModel.getAuc());
	}
	
	public void close() {
		printStream.close();
	}

	public void writeOptions(Options vOptions) {
		printStream.println("#option:"+vOptions.optionId+";classificator:"+vOptions.classificator+";kernel:"+vOptions.kernelInstance.getClass().getName().replace("ma.kernel.", "")+"("+vOptions.kernel+");ratio:"+vOptions.ratio+";set sizes(train,expand,addition,test):"+vOptions.noOfTrainingInstances+"/"+vOptions.expansionSize+"/"+vOptions.noOfNewTrainingInstances+"/"+vOptions.noOfTestinstances+";lambda:"+vOptions.lambda+";randomExpansion:"+vOptions.randomRatio+";label:"+vOptions.label+";normalized:"+(vOptions.normalize?"1":"0")+";SVM_C:"+vOptions.svm_c);
	}
}
