package ma.tools.files;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import ma.Options;
import ma.models.ClassificationResultModel;
import ma.models.TextModel;

public class SimpleFileWriter {
	private PrintStream printStream;
	public SimpleFileWriter(File dir) {
		try {
			printStream = new PrintStream(dir);
		} catch (IOException e3) {
			e3.printStackTrace();
		}
	}
	
	public void writeValue(Number value) {
		printStream.println(value);
	}
	
	public void writeValues(Number[] values) {
		for(int i=0; i<values.length;i++) printStream.print(values[i]+" ");
		printStream.println();
	}
	
	public void close() {
		printStream.close();
	}
}
