package ma.tools.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import ma.annotations.Measure;
import ma.annotations.Trace;
import ma.models.ClassificationResultModel;
import ma.models.TextModel;
import ma.tools.GNUPlotDocument;
import ma.tools.Sorter;

import com.panayotis.gnuplot.plot.DataSetPlot;
import com.panayotis.gnuplot.style.Style;

public class FileHandler {
	public static boolean createDataFiles = true;
	
	@Measure
	public static void createDataFile(double[][] data, File file) {
		FileWriter fileWriter;
		try {
			fileWriter = new FileWriter(file);
			for(int i=0; i<data.length;i++) {
				String line = Double.toString(data[i][0]);
				for(int j=1; j<data[i].length;j++) {
					line+=" "+Double.toString(data[i][j]);
				}
				fileWriter.write(line+"\n");
			}
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void moveFile(String origin, String destination) {
		moveFile(new File(origin), new File(destination));
	}
	
	@Measure
	public static void moveFile(File origin, File destination) {
		origin.renameTo(destination);
	}
	
	public static File createPrecomputedKernelMatrixFile(String filename, double[][] data) {
		return createPrecomputedKernelMatrixFile(filename, data, null);
	}
	
	@Measure
	public static File createPrecomputedKernelMatrixFile(String filename, double[][] data, DecimalFormat format) {
		FileWriter fileWriter;
		try {
			File file = new File(filename);
			fileWriter = new FileWriter(file);
			fileWriter.write(data.length+" "+data.length+"\n");
			for(int i=0; i<data.length;i++) {
				for(int j=0; j<data[i].length;j++) {
					String number = Double.toString(data[i][j]);
					if(format!=null) {
						number = format.format(data[i][j]);
					}
					fileWriter.write(number+"\t");
				}
				fileWriter.write("\n");
			}
			fileWriter.close();
			return file;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
		
	}

	@Measure
	public static void createGraph(File dataSource, File destination, int type) {
		if(dataSource==null || destination==null || !dataSource.exists()) {
			return;
		}
		
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(dataSource));
			LinkedList<int[]> dataset = new LinkedList<int[]>();
			String line;
			while(((line = bufferedReader.readLine()) != null)) {
				String[] data = line.split(";");
				int[] entry = new int[2];
				entry[0] = Integer.parseInt(data[0]);
				entry[1] =  Integer.parseInt(data[1]);
				dataset.add(entry);
			}
			bufferedReader.close();
			int[][] data = new int[dataset.size()][];
			for(int i=0; i<dataset.size();i++) {
				data[i] = dataset.get(i);
			}
			
			int[][] randomData = {{0,0}, {data[data.length-1][0], data[data.length-1][1]}};
			
			GNUPlotDocument gnuPlotDocument = new GNUPlotDocument(destination);
			gnuPlotDocument.addPlot(new DataSetPlot(randomData), "random", Style.LINES, 1, 0, 0);
			gnuPlotDocument.addPlot(new DataSetPlot(data), "RoC-Curve", Style.LINESPOINTS, 1, 1, 1);
			
			gnuPlotDocument.createImage();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Trace
	@Measure
	public static double createRoCCurveandDistribution(double[] probabilities, int[] classes, File destination) {
		if(destination==null || probabilities==null || classes==null || probabilities.length!=classes.length) {
			return Double.MAX_VALUE;
		}
		int dataLength = classes.length;
		
		int[] sortIndices = Sorter.treeMapSort(probabilities);
		int pos = 0;
		int neg = 0;
		double minDist = 1.0;
		for(int i=0; i<sortIndices.length; i++) {
			if(classes[i]==1)pos++; else neg++;
			if(i<sortIndices.length-1 && probabilities[sortIndices[i+1]]-probabilities[sortIndices[i]]<minDist) {
				minDist = probabilities[sortIndices[i+1]]-probabilities[sortIndices[i]];
			}
		}
		
		/**
		 * Implementation from HPL-2003-4-RoC-Introduction p. 6
		 */
		LinkedList<double[]> rocData = new LinkedList<>();
		double[] point0 = {0,0};
		rocData.add(point0);
		if(pos!=0 && neg!=0) {
			for(int p=dataLength-1; p>=0;p--) {
				int fp=0;
				int tp=0;
				for(int i=0; i<dataLength;i++) {
					if(probabilities[i]>probabilities[sortIndices[p]]) {
						if(classes[i]==1) tp++; else fp++;
					}
				}
				double[] point = {fp*1.0/neg, tp*1.0/pos};
				rocData.add(point);
			}
		}
		double[] point1 = {1,1};
		rocData.add(point1);

		double auc = 0;
		
		double[][] rocDataList = new double[rocData.size()][];
		double[] previousPoint = {0,0};
		for(int i=0; i<rocData.size(); i++) {
			rocDataList[i] = rocData.get(i);
			if(previousPoint[0]!=rocDataList[i][0]) {
				double rect = (rocDataList[i][0]-previousPoint[0])*previousPoint[1];
				double tri = (rocDataList[i][0]-previousPoint[0])*(rocDataList[i][1]-previousPoint[1])/2;
				auc+=rect+tri;
			}
			previousPoint = rocDataList[i];
		}		
//			System.out.println();
		/**
		 * Distribution curve
		 */
		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;
		//redundant but try to seperate from above
		for(int i=0; i<dataLength;i++) {
			if(probabilities[i]<min) {
				min = probabilities[i]; 
			}
			if(probabilities[i]>max) {
				max = probabilities[i]; 
			}
		}
		
		int log = 4;
		int steps = (int)Math.round(dataLength*Math.log(log)/Math.log(dataLength));
		double distance = max-min;
		double stepSize = distance/steps;
		
		int[] counter = new int[steps];
//		int countErrors = 0;
		for(int i=0; i<dataLength;i++) {
			int index = -1;
			index = (int)Math.round(Math.floor((probabilities[i]-min)/stepSize));
			if(index<counter.length) {
				counter[index]++;
//			} else {
//				countErrors++;
			}
			
		}
/*		System.out.println("Steps: "+steps+", Step size: "+stepSize+", Errornous index: "+countErrors);
		System.out.println();*/
		double[][] distribution = new double[counter.length][];
		for(int i=0; i<counter.length; i++) {
			double[] t = {i*stepSize+min, counter[i]*1.0/probabilities.length};
			distribution[i] = t;
		}
		
		if(createDataFiles) {
			createDataFile(rocDataList, new File(destination.getPath()+".dat"));
		}
		
		double[][] randomPlot = {{0,0},{1,1}};
		
		GNUPlotDocument gnuPlotDocument = new GNUPlotDocument(destination);
		gnuPlotDocument.defineAxis(0, 1, 0, 1);
		gnuPlotDocument.addPlot(new DataSetPlot(distribution), "Prob. distr.", Style.LINES, 1, 0, 0);
		gnuPlotDocument.addPlot(new DataSetPlot(rocDataList), "RoC-Curve ("+auc+")", Style.LINESPOINTS, 1, 1, 1);
		gnuPlotDocument.addPlot(new DataSetPlot(randomPlot), "random", Style.LINES, 1, 0, 0);
		gnuPlotDocument.createImage();
		
		return auc;
	}
	
	@Trace
	@Measure
	public static double createPRCurve(double[] probabilities, int[] classes, File destination) {
		if(destination==null || probabilities==null || classes==null || probabilities.length!=classes.length) {
			return Double.MAX_VALUE;
		}
		int dataLength = classes.length;
		
		int[] sortIndices = Sorter.treeMapSort(probabilities);
		int pos = 0;
		int neg = 0;
		double minDist = 1.0;
		for(int i=0; i<sortIndices.length; i++) {
			if(classes[i]==1)pos++; else neg++;
			if(i<sortIndices.length-1 && probabilities[sortIndices[i+1]]-probabilities[sortIndices[i]]<minDist) {
				minDist = probabilities[sortIndices[i+1]]-probabilities[sortIndices[i]];
			}
		}
		
		/**
		 * Implementation from HPL-2003-4-RoC-Introduction p. 6
		 */
		LinkedList<double[]> prData = new LinkedList<>();
//		prData.add(point0);
		if(pos!=0 && neg!=0) {
			for(int p=dataLength-1; p>=0;p--) {
				int fp=0;
				int tp=0;
				for(int i=0; i<dataLength;i++) {
					if(probabilities[i]>=probabilities[sortIndices[p]]) {
						if(classes[i]==1) tp++; else fp++;
					}
				}
				double[] point = {tp*1.0/pos, tp*1.0/(tp+fp)};
				prData.add(point);
			}
		}

		double auc = 0;
		
		double[][] prDataList = new double[prData.size()][];
		double[] previousPoint = null;
		for(int i=0; i<prData.size(); i++) {
			prDataList[i] = prData.get(i);
			if(previousPoint!=null && previousPoint[0]!=prDataList[i][0]) {
				double rect = (prDataList[i][0]-previousPoint[0])*previousPoint[1];
				double tri = (prDataList[i][0]-previousPoint[0])*(prDataList[i][1]-previousPoint[1])/2;
				auc+=rect+tri;
			}
			previousPoint = prDataList[i];
		}		
//			System.out.println();
				
		if(createDataFiles) {
			createDataFile(prDataList, new File(destination.getPath()+".dat"));
		}
		
		double[][] randomPlot = {{0,0},{1,1}};
		
		GNUPlotDocument gnuPlotDocument = new GNUPlotDocument(destination);
		gnuPlotDocument.defineAxis(0, 1, 0, 1);
		if(prDataList.length>0) {
			gnuPlotDocument.addPlot(new DataSetPlot(prDataList), "PR-Curve ("+auc+")", Style.LINESPOINTS, 1, 1, 1);
		}
		gnuPlotDocument.addPlot(new DataSetPlot(randomPlot), "random", Style.LINES, 1, 0, 0);
		gnuPlotDocument.createImage();
		
		return auc;
	}

	@Trace
	@Measure
	public static void createProbabilityDistribution(double[] fValues, double[] probabilities, File destination) {
		if(destination==null || fValues==null || probabilities==null || fValues.length!=probabilities.length) {
			return;
		}
		double[][] data = new double[fValues.length][];
		for(int i=0; i<fValues.length;i++) {
			data[i] = new double[2];
			data[i][0] = fValues[i];
			data[i][1] = probabilities[i];
		}
		
		if(createDataFiles) {
			createDataFile(data, new File(destination.getPath()+".dat"));
		}
		GNUPlotDocument gnuPlotDocument = new GNUPlotDocument(destination);
		gnuPlotDocument.addPlot(new DataSetPlot(data), "", Style.POINTS, 0, 1, 1);
		gnuPlotDocument.createImage();
	}

	public static <T extends TextModel> void createArffFile(String prefix, List<T> sentences, String label) {
		createArffFile(null, prefix, sentences, label);
	}
	
	@Measure
	public static <T extends TextModel> void createArffFile(File outputDirectory, String prefix, List<T> entries, String label) {
		String indices = new String();
		String dataLinesPoly = new String();
//		String dataLinesSSK = new String();
		
		for(int i=0; i<entries.size(); i++) {
			T entry = entries.get(i);
			dataLinesPoly+="row"+entry.getId()+","+(entry.getLabels().contains(label)?"1":"-1")+"\n";
//			dataLinesSSK+="row"+entry.getId()+","+(entry.getLabels().contains(label)?"1":"-1")+",\""+entry.getCleanedText().replace("\"", "\\\"")+"\""+"\n";
			indices+="row"+entry.getId()+", ";
		}
		try {
			String path = "";
			if(outputDirectory!=null) path = outputDirectory.getPath()+"/";
			FileWriter dataOutput = new FileWriter(path+prefix+"_data.arff");
			dataOutput.write("@relation "+prefix+"\n");
			dataOutput.write("@attribute index {"+indices.substring(0, indices.length()-2)+"}"+"\n");
			dataOutput.write("@attribute class {-1,1}"+"\n");
			dataOutput.write("@data"+"\n");
			dataOutput.write(dataLinesPoly);
			dataOutput.close();
			/*
			FileWriter dataOutputSSK = new FileWriter(path+prefix+"_SSKData.arff");
			dataOutputSSK.write("@relation "+prefix+"\n");
			dataOutputSSK.write("@attribute index {"+indices.substring(0, indices.length()-2)+"}"+"\n");
			dataOutputSSK.write("@attribute class {-1,1}"+"\n");
			dataOutputSSK.write("@attribute text string"+"\n");
			dataOutputSSK.write("@data"+"\n");
			dataOutputSSK.write(dataLinesSSK);
			dataOutputSSK.close();*/
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@Measure
	public static <T extends TextModel> void createNDimensionalArffFile(List<T> testSet, int countMinimum, String prefix, File directory, String label) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader("data/allLemmataCount.txt"));	
			String line = null;
			LinkedList<String> bag = new LinkedList<String>();
			while((line=reader.readLine())!=null) {
				String[] data = line.split(" ");
				if(Integer.parseInt(data[1])>=countMinimum) {
					bag.add(data[0]);
				}
			}
			reader.close();
			
			//getting ids
			HashSet<String> ids = new HashSet<>();
			for(TextModel textModel:testSet) {
				ids.add(""+textModel.getId());
			}
			
			FileWriter arffWriter = new FileWriter(directory.getPath()+"/"+prefix+"_"+countMinimum+"dim.arff");
			arffWriter.write("@relation testData\n\n");
			arffWriter.write("@attribute index {"+ids.toString().replaceAll("[\\[\\]]", "")+"}\n");
			arffWriter.write("@attribute class {-1,1}"+"\n");
			for(String lemma:bag) {
				arffWriter.write("@attribute "+lemma+" {0,1}\n");
			}
			arffWriter.write("\n@data\n");
			for(TextModel textModel:testSet) {
				Set<String> lemmata = textModel.getLemmata();
				
				String outputLine = "{0 "+textModel.getId()+", 1 "+(textModel.getLabels().contains(label)?"1":"-1");

				for(int i=0;i<bag.size(); i++) {
					if(lemmata.size()==0) break;
					String lemma = bag.get(i);
					if(lemmata.contains(lemma)) {
						lemmata.remove(lemma);
						outputLine+=", "+(i+2)+" 1";
					}
				}
				outputLine+="}\n";
				arffWriter.write(outputLine);
			}
			arffWriter.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Measure
	public static <T extends TextModel> void exportClassificationResults(ClassificationResultModel<T> classificationResultModel, File file) {
		try {
			FileWriter writer = new FileWriter(file);
			for(int i=0; i<classificationResultModel.classes.length;i++) {
				writer.write(classificationResultModel.classes[i]+" "+classificationResultModel.classifiedClasses[i]+" "+classificationResultModel.probabilities[i]+" "+classificationResultModel.fValues[i]+"\n");
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void deleteTree(File path) {
		deleteTree(path, null);
	}
		
	/**
	 * 
	 * @param path
	 * @param filter Regex for not deletable files; side effect: directories won't be deleted
	 */
	public static void deleteTree(File path, String filter) {
		for (File file : path.listFiles()) {
			if (file.isDirectory() && filter==null) {
				deleteTree(file, filter);
			} else if(file.isFile() && (filter==null || !file.getName().matches(filter))) {
				if (!file.delete()) System.err.println(file + " could not be deleted!");
			}
		}
		if (filter==null) {
			if(!path.delete()) System.err.println(path + " could not be deleted!");
		}
	}

}
