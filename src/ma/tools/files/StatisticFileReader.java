package ma.tools.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ma.models.ClassificationResultModel;
import ma.models.TextModel;

public class StatisticFileReader {
	private BufferedReader reader;
	//Iteration/Setup/Results(SVM, kNN)
	public Map<String, List<ClassificationResultModel<TextModel>>> entries = new LinkedHashMap<>();
//	public List<List<List<ClassificationResultModel<TextModel>>>> entries = new LinkedList<>();
	public Map<String, Map<String, String>> setupData = new LinkedHashMap<>();
	private int iterationCount = 0;
	
	public int getSetupCount() {
		return entries.size();
	}
	
	public int getIterationCount() {
		return iterationCount;
	}
	
	public int getIterationCount(String pSetupId) {
		return entries.get(pSetupId).size();
	}
	
	public StatisticFileReader(File dir) {
		try {
			reader = new BufferedReader(new FileReader(dir));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		parseFile();
	}
	
	private boolean parseFile() {		
		try {
			String line = reader.readLine(); //init with head line
			while((line = reader.readLine())!=null) {
				if(line.startsWith("#")) {
					String[] setupFields = line.substring(1).split(";");
					Map<String, String> setup = new HashMap<String, String>();
					for(int i=0; i<setupFields.length;i++) {
						String[] entry = setupFields[i].split(":");
						setup.put(entry[0], entry[1]);
					}
					if(!setup.containsKey("option")) {
						System.out.println("Option string invalid. No option id found.");return false;
					}
					entries.put(setup.get("option"), new LinkedList<ClassificationResultModel<TextModel>>());
					
					setupData.put(setup.get("option"), setup);
					continue;
				}
				String[] fields = line.split(";");
				if(!entries.containsKey(fields[1])) {
					System.out.println("Entry string invalid. No corresponding option found."); return false;
				}
				ClassificationResultModel<TextModel> tempResultModel = new ClassificationResultModel<TextModel>(0);
				tempResultModel.tp = Double.parseDouble(fields[3]);
				tempResultModel.fp = Double.parseDouble(fields[4]);
				tempResultModel.tn = Double.parseDouble(fields[5]);
				tempResultModel.fn = Double.parseDouble(fields[6]);
				tempResultModel.auc = Double.parseDouble(fields[15]);
//				tempResultModel.tag = fields[15];
				entries.get(fields[1]).add(tempResultModel);
				iterationCount = entries.get(fields[1]).size();
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}