package ma.tools;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class MeasureCollector {
	private static HashMap<String, List<Long>> measures = new HashMap<>();
	private static int countCap = 1000;
	
	public static void init() {
		measures = new HashMap<>();
	}
	
	public static void measure(String code, long time) {
		if(!measures.containsKey(code)) {
			measures.put(code, new LinkedList<Long>());
		}
		//Removes first element
		if(measures.get(code).size()>=countCap) measures.get(code).remove(0);
		measures.get(code).add(time);
	}
	
	public static double getMean(String code) {
		if(!measures.containsKey(code)) {
			return 0;
		}
		double value = 0;
		for(Long v:measures.get(code)) {
			value+=v;
		}
		return value/measures.get(code).size();
	}
	
	public static Set<String> getMeasureCodes() {
		return measures.keySet();
	}

	public static Long getSum(String code) {
		Long sum = new Long(0);
		for(Long l:measures.get(code)) {
			sum+=l;
		}
		return sum;
	}
	
	public static Long getMedian(String code) {
		List<Long> l= measures.get(code);
		Collections.sort(l);
		return l.size()>0?l.get(l.size()/2):0;
	}
	
	public static int getSize(String code) {
		return measures.get(code).size();
	}
}
