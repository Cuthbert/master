package ma.kernel;

import edu.berkeley.nlp.syntax.Tree;

public class SyntacticTreeKernel extends BasicTreeKernel {
	private double lambda = 1;
	

	public SyntacticTreeKernel() {
		super(0);
		kernelId=1;
	}
	
	public SyntacticTreeKernel(int mode) {
		super(mode);
		kernelId=1;
	}
	
//	public void init(Tree<String> tree1, Tree<String> tree2) {
//		setTrees(tree1, tree2);
//	}
	
	@SuppressWarnings("unchecked")
	@Override
	public double delta(Tree<String> n1, Tree<String> n2) {
		double value=lambda;
		//if(n2==null) return 0;
		
		if(n1.isPreTerminal()&&n2.isPreTerminal()) {
			if(useLemma && n1.getTerminals().size()>0 && n2.getTerminals().size()>0) {
				if(n1.getTerminals().get(0).getLabel().length()>0 && n1.getTerminals().get(0).getLabel().equals(n2.getTerminals().get(0).getLabel())) {
					value*=1+value;
				}
			}
			return value;
		}
		Tree<String>[] children1 = new Tree[n1.getChildren().size()];
		children1 = n1.getChildren().toArray(children1);
		Tree<String>[] children2 = new Tree[n2.getChildren().size()];
		children2 = n2.getChildren().toArray(children2);
		for(int i=0; i<children1.length;i++) {
			if(i>=children2.length) break;
			if(!children1[i].getLabel().equals(children2[i].getLabel())) continue; //instead of calling delta with differing label and returning 0
			
			value*=(1+delta(children1[i], children2[i]));
		}
		return value;
	}
}