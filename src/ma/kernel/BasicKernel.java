package ma.kernel;

import java.util.HashMap;

import ma.annotations.Measure;
import ma.models.TextModel;

public abstract class BasicKernel {
	protected int kernelId = -1; 
	protected int kernelDataType = -1; //1=Sentence, 2=Entry
	
	public HashMap<String, String> optionMap = new HashMap<String, String>(); 
	
	public abstract void init(TextModel sentence1, TextModel sentence2);
	@Measure
	public abstract double compute();

	public int getKernelId() {
		return kernelId;
	}
	
	public int getKernelDataType() {
		return kernelDataType;
	}
}
