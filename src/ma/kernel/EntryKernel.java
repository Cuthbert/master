package ma.kernel;

import ma.models.EntryModel;
import ma.models.TextModel;
/**
 * Kernel based on adding kernel values per sentence pair up
 * @author cuthbert
 *
 */
public class EntryKernel extends BasicKernel {
	private EntryModel entry1;
	private EntryModel entry2;
	protected BasicKernel sentenceKernel;
	
	public EntryKernel() {
		kernelId = 100;
		kernelDataType = 2;
	}
	
	public EntryKernel(BasicKernel kernel) {
		this();
		sentenceKernel = kernel;
		kernelId+=kernel.getKernelId();
	}
	
	public void init(TextModel entry1, TextModel entry2) {
		this.entry1 = (EntryModel)entry1;
		this.entry2 = (EntryModel)entry2;
	}
	
	public void init(BasicKernel kernel) {
		sentenceKernel = kernel;
	}
	
	public double compute() {
		double value = 0;
		for(int i=0; i<entry1.getSentences().size();i++) {
			for(int j=i; j<entry2.getSentences().size();j++) {
				this.sentenceKernel.init(entry1.getSentences().get(i), entry2.getSentences().get(j));
				value+=sentenceKernel.compute()*(i!=j?2:1);
			}
		}
		return value;///(entry1.getSentences().size()*entry2.getSentences().size());
	}
}
