package ma.kernel;

import java.util.HashMap;
import java.util.Set;

import ma.models.AnnotationModel;
import ma.models.SentenceModel;
import ma.models.TextModel;

public class BOWStringKernel extends BasicKernel {
	String text1 = null;
	String text2 = null;
	SentenceModel sentence1 = null;
	SentenceModel sentence2 = null;
	Set<String> bag = null;
	boolean useLemma = false;
	
	public BOWStringKernel() {
		kernelId=3;
		kernelDataType = 1;
	}
	
	public BOWStringKernel(int mode) {
		this();
		switch(mode) {
			case 0:
				useLemma = false;
				break;
			case 1:
				useLemma = true;
				break;
		}
	}
	
	@Override
	public void init(TextModel sentence1, TextModel sentence2) {
		this.sentence1 = (SentenceModel)sentence1;
		this.sentence2 = (SentenceModel)sentence2;
	}

	public void setBag(Set<String> bag) {
		this.bag = bag;
	}
	
	private HashMap<String, Integer> getWordCounts(SentenceModel sentenceModel) {
		HashMap<String, Integer> wordCounter = new HashMap<String, Integer>();
		
		for(AnnotationModel word:sentenceModel.getAnnotations()) {
			String modWord = useLemma?word.getLemma().replaceAll("[\\.\\?!;]", "").toLowerCase():word.getText().replaceAll("[\\.\\?!;]", "").toLowerCase();
			if(!modWord.matches("^[a-z]*$") || modWord.length()<=3) continue; 
			if(!wordCounter.containsKey(modWord)) {
				wordCounter.put(modWord, 0);
			}
			wordCounter.put(modWord, new Integer(wordCounter.get(modWord)+1));
		}
		return wordCounter;
	}
	
	@Override
	public double compute() {
//		if(bag==null) return 0;
		//collect words with counts
		HashMap<String, Integer> wordCounter1 = getWordCounts(sentence1);
		HashMap<String, Integer> wordCounter2 = getWordCounts(sentence2);
		
		double result = 0;
		//use only common keys
		wordCounter1.keySet().retainAll(wordCounter2.keySet());
		for(String key:wordCounter1.keySet()) {
			result+=wordCounter1.get(key)*wordCounter2.get(key);
		}
		return result;
	}

}
