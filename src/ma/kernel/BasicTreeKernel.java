package ma.kernel;

import java.util.List;

import ma.models.SentenceModel;
import ma.models.TextModel;
import edu.berkeley.nlp.syntax.Tree;

public abstract class BasicTreeKernel extends BasicKernel{
	protected Tree<String> tree1 = null;
	protected Tree<String> tree2 = null;
	protected boolean useLemma = false;
	
	/**
	 * Child classes must implement fragment set
	 * @param tree1
	 * @param tree2
	 */
	public BasicTreeKernel() {
		kernelDataType = 1;
	}
	
	public BasicTreeKernel(int mode) {
		this();
		switch(mode) {
			case 0:
				useLemma=false;
				break;
			case 1:
				useLemma=true;
				break;
		}
	}
	
	@Override
	public void init(TextModel sentence1, TextModel sentence2) {
		setTrees(((SentenceModel)sentence1).getTree(useLemma), ((SentenceModel)sentence2).getTree(useLemma));
	}
	
	public abstract double delta(Tree<String> n1, Tree<String> n2);
	
	public void setTrees(Tree<String> tree1, Tree<String> tree2) {
		this.tree1=tree1;
		this.tree2=tree2;
	}
	
	@Override
	public double compute() {
		return compute(false);
	}
	
	public double compute(boolean debug) {
		double value = 0;
		//Liste der Subtrees auch gleichzeit Liste der Nodes, Kinder werden aber für Delta (hier) nicht betrachtet
		List<Tree<String>> nodes1 = STFProducer.produceSubtrees(tree1);
		List<Tree<String>> nodes2 = STFProducer.produceSubtrees(tree2);
		
		for(Tree<String> node1:nodes1) {
			for(Tree<String> node2:nodes2) {
				double tempValue = delta(node1, node2); 
				value+=tempValue;
			}
		}
		
		return value;
	}
}