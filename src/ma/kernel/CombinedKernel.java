package ma.kernel;

import ma.models.TextModel;

public class CombinedKernel extends BasicKernel {
	private BasicKernel kernel1;
	private BasicKernel kernel2;
	private double lambda;
	
	public CombinedKernel(double lambda, BasicKernel kernel1, BasicKernel kernel2) {
		super();
		this.lambda = lambda;
		this.kernel1 = kernel1;
		this.kernel2 = kernel2;
		kernelDataType = kernel1.kernelDataType==kernel2.kernelDataType?kernel1.kernelDataType:-1;
	}
	
	@Override
	public void init(TextModel sentence1, TextModel sentence2) { 
		kernel1.init(sentence1, sentence2);
		kernel2.init(sentence1, sentence2);
	}

	@Override
	public double compute() {
		return kernel1.compute()*lambda+kernel2.compute()*(1-lambda);
	}
}