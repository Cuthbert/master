package ma.kernel;

import java.util.HashMap;

import ma.models.AnnotationModel;
import ma.models.EntryModel;
import ma.models.SentenceModel;
import ma.models.TextModel;
/**
 * Kernel based on adding kernel values per sentence pair up
 * @author cuthbert
 *
 */
public class EntryBOWLemmaKernel extends BasicKernel {
	private EntryModel entry1;
	private EntryModel entry2;
	
	public EntryBOWLemmaKernel() {
		kernelId = 200;
		kernelDataType = 2;
	}
	
	public void init(TextModel entry1, TextModel entry2) {
		this.entry1 = (EntryModel)entry1;
		this.entry2 = (EntryModel)entry2;
	}
	
	private HashMap<String, Integer> getWordCounts(EntryModel entryModel) {
		HashMap<String, Integer> wordCounter = new HashMap<String, Integer>();
		
		for(SentenceModel sentenceModel:entryModel.getSentences()) {
			for(AnnotationModel word:sentenceModel.getAnnotations()) {
				String modWord = word.getLemma().replaceAll("[\\.\\?!;]", "").toLowerCase();
				if(!modWord.matches("^[a-z]*$") || modWord.length()<=3) continue; 
				if(!wordCounter.containsKey(modWord)) {
					wordCounter.put(modWord, 0);
				}
				wordCounter.put(modWord, new Integer(wordCounter.get(modWord)+1));
			}
		}
		return wordCounter;
	}
	
	public double compute() {
		HashMap<String, Integer> wordCounter1 = getWordCounts(entry1);
		HashMap<String, Integer> wordCounter2 = getWordCounts(entry2);
		
		double result = 0;
		//use only common keys
		wordCounter1.keySet().retainAll(wordCounter2.keySet());
		for(String key:wordCounter1.keySet()) {
			result+=wordCounter1.get(key)*wordCounter2.get(key);
		}
		return result;
	}
}
