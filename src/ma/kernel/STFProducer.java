package ma.kernel;

import java.util.ArrayList;
import java.util.List;

import edu.berkeley.nlp.syntax.Tree;

public class STFProducer {
	/**
	 * Returns all subtrees, except of leaves 
	 * @param tree
	 * @return
	 */
	public static List<Tree<String>> produceSubtrees(Tree<String> tree) {
		return produceSubtrees(tree, true);
	}
	/**
	 * Returns all subtrees
	 * @param tree
	 * @return
	 */
	public static List<Tree<String>> produceSubtrees(Tree<String> tree, boolean pruneLeaves) {
		List<Tree<String>> subtrees = new ArrayList<Tree<String>>();
		subtrees.add(tree);
		for(int i=0; i<subtrees.size(); i++) {
			for(Tree<String> subtree:subtrees.get(i).getChildren()) {
				if(subtree.isLeaf() && pruneLeaves) continue;
				subtrees.add(subtree);
			}
		}
		return subtrees;
	}
}