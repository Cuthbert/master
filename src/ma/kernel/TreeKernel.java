package ma.kernel;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ma.models.SentenceModel;
import ma.models.TextModel;
import edu.berkeley.nlp.syntax.Tree;
@Deprecated
public class TreeKernel extends BasicTreeKernel {
	private static HashMap<String, List<Tree<String>>> cache = new HashMap<String, List<Tree<String>>>();
	protected Set<Tree<String>> fragmentSpace = new HashSet<Tree<String>>();
	
	@Override
	public void init(TextModel sentence1, TextModel sentence2) {
		Tree<String> tree1 = ((SentenceModel)sentence1).getTree();
		Tree<String> tree2 = ((SentenceModel)sentence2).getTree();
		cache = new HashMap<>();
		//Liste der Subtrees
		String tree1Key = tree1.toString();
		String tree2Key = tree2.toString();
		List<Tree<String>> subtrees1 = cache.get(tree1Key);
		List<Tree<String>> subtrees2 = cache.get(tree2Key);
		if(subtrees1==null) {
			subtrees1 = STFProducer.produceSubtrees(tree1);
			cache.put(tree1Key, subtrees1);
		}
		if(subtrees2==null) {
			subtrees2 = STFProducer.produceSubtrees(tree2); 
			cache.put(tree2Key, subtrees2);
		}
		
		fragmentSpace.addAll(subtrees1);
		fragmentSpace.addAll(subtrees2);
	}
	
	@Override
	public double delta(Tree<String> n1, Tree<String> n2) {
		double value=0;
		for(Tree<String> fragment:fragmentSpace) {
			String label = fragment.getLabel(); 
			value+= (label.equals(n1.getLabel())&&label.equals(n2.getLabel()))?1:0;
		}
		return value;
	}
	
	public Set<Tree<String>> getFragmentSpace() {
		return fragmentSpace;
	}

	public void setFragmentSpace(Set<Tree<String>> fragmentSpace) {
		this.fragmentSpace = fragmentSpace;
	}
}
