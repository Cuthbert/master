package gate;

import gate.util.GateException;
import gate.util.persistence.PersistenceManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;

public class GermanTokenizerGateOnly {
	public static void main(String[] args) {
		String sourceFileName = "/home/cuthbert/Dropbox/Uni/Masterarbeit/Daten/prime_automotive.arff";
		BufferedReader reader;
		FileWriter logFileWriter;
		FileWriter docWriter;
		try {
			//init file
			File sourceFile = new File(sourceFileName);
			reader = new BufferedReader(new FileReader(sourceFile));
			logFileWriter = new FileWriter(sourceFile.getAbsolutePath()+".log");
			docWriter = new FileWriter(sourceFile.getAbsolutePath()+".txt");
			ArffReader arff = new ArffReader(reader);
			Instances data = arff.getData();
			//init GATE
			Gate.setGateHome(new File("/home/cuthbert/Programme/GATE_Developer_8.0"));
			Gate.init();
			
			CorpusController controller = (CorpusController)PersistenceManager.loadObjectFromFile(new File("/home/cuthbert/Dropbox/Uni/Masterarbeit/german-tagging.gapp"));
			controller.setCorpus(Factory.newCorpus("automotive"));
			//build corpus
			/*for (int i=0; i<data.numInstances() && i<10; i++) {
				String text = data.instance(i).stringValue(0);
//				System.out.println(data.instance(i).stringValue(0));
				Document document = Factory.newDocument(text);
				docWriter.write(text+"\n");
				controller.getCorpus().add(document);
			}*/
			for (int i=0; i<data.attribute(0).numValues() && i<10; i++) {
				String text = data.attribute(0).value(i);
//				System.out.println(data.instance(i).stringValue(0));
				Document document = Factory.newDocument(text);
				docWriter.write(text+"\n");
				controller.getCorpus().add(document);
			}
			for(int i=0; i<controller.getCorpus().size();i++) {
				System.out.println(controller.getCorpus().getDocumentName(i));
			}
			//run
			controller.execute();
			Corpus corpus = controller.getCorpus(); 
			for(int i=0; i<corpus.size();i++) {
				AnnotationSet annotationSet = corpus.get(i).getAnnotations();
				logFileWriter.write(corpus.get(i).getContent().toString()+"\n");
				for(Annotation annotation: annotationSet) {
					if(!annotation.getType().equals("Sentence")) continue;
					logFileWriter.write(annotation.toString());
					/*System.out.println("AnnotationType: "+annotation.getType());
					for(Object key :annotation.getFeatures().keySet()) {
						System.out.println("\t"+key+": "+annotation.getFeatures().get(key));
					}
					System.out.println();*/
				}
				logFileWriter.write("\n");
			}
			logFileWriter.close();
			docWriter.close();
			reader.close();
		} catch (GateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

